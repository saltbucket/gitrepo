﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine; //DEBUG

public class ShipBehaviour
{
    protected AIShipController con;
    public string doWith;
    public bool active;
    public string name;

    public AIStateMachine thisMachine;
    protected List<AIStateMachine> otherMachines;
    
    //  construct base class (ran from inhereting class)
    protected void ShipBehaviourConstructor(AIShipController cntrllr, AIStateMachine thsMchn, string dWth, List<AIStateMachine> othrMchns)
    {
        con = cntrllr;
        doWith = dWth;
        thisMachine = thsMchn;
        otherMachines = othrMchns;
    }

    //  run next state in given machines
    protected void StateComplete()
    {
        con.Thrust(false);

        if (thisMachine != null)
        {
            thisMachine.CycleStates();
        }

        if (otherMachines != null)
        {
            foreach (AIStateMachine stateMachine in otherMachines)
            { stateMachine.CycleStates(); }
        }
    }

    //  make doWith available in interface type IBehaviour
    public string DoWith()
    {
        return doWith;
    }

    public virtual void Enter() { }
    public virtual void Update() { }
    public virtual void SlowUpdate() { }
    public virtual void SlowerUpdate() { }
    public virtual void Exit() { }
}
