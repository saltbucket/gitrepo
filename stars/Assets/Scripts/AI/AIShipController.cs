﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIShipController : Pilot
{
    public AIStateMachine aiState;

    [SerializeField]
    public List<string> debugBehaviourList = new List<string>();//DEBUG
    public List<string> debugSubBehaviourList = new List<string>();//DEBUG


    [Header("- Geometry")]

    //  direction
    public Quaternion directionToTarget;
    public Quaternion directionToPoint;

    //  angle
    public float angleToTarget;
    public float angleToPoint;

    //  distance
    public float distanceToTarget;
    public float distanceToPoint;

    //  speed
    public float currentSpeed;
    public Vector2 leaderVelocity;

    public SystmObject currentLeader;
    
    public void DebugAI(List<ShipBehaviour> queue, List<string> debug)//DEBUG
    {
        debug.Clear();

        foreach (ShipBehaviour behaviour in queue)
        {
            string[] sArr = behaviour.ToString().Split('+');

            debug.Add(sArr[sArr.Length - 2] + " - " + sArr[sArr.Length - 1]);
        }
    }

    public Vector2 gizTarget;
    public Vector2 gizOrigin;
    public void SetGizMo(Vector2 target, Vector2 origin)
    {
        gizTarget = target;
        gizOrigin = origin;
    }
    private void OnDrawGizmos()
    {
        if (gizOrigin == null || gizTarget == null)
        {
            return;
        }
        Gizmos.DrawLine(gizTarget, gizOrigin);
    }


    //DEBUG

    public override void RefreshShip()
    {
        if (!autopilot)
        {
            base.RefreshShip();
        }
    }

    private void Awake()
    {
        ai = true;
        //  this has to go in here/player controller
        RefreshShip();
        //  create state machine
        aiState = new AIStateMachine(this, true);
    }

    //  //
    #region Geometry Updates 
    //  //

    //  target
    public void DirectionToTarget()
    { directionToTarget = SystmAPI.Geometry.GetDirection((Vector2) ship.target.transform.position, shipTrans.position); }

    public void DistanceToTarget()
    { if (ship.target != null) { distanceToTarget = Vector3.Distance(ship.target.transform.position, transform.position); } }

    public void AngleToTarget()
    { angleToTarget = Quaternion.Angle(shipTrans.rotation, SystmAPI.Geometry.GetDirection(ship.target.transform.position, shipTrans.position));}

    // point
    public void DistanceToPoint(Vector2 point)
    { distanceToPoint = Vector3.Distance(point, transform.position); }

    public void DirectionToPoint(Vector2 point)
    { directionToPoint = SystmAPI.Geometry.GetDirection(point, shipTrans.position); }

    public void AngleToPoint(Vector2 point)
    { angleToPoint = Quaternion.Angle(shipTrans.rotation, SystmAPI.Geometry.GetDirection(point, shipTrans.position)); }

    // other
    public void CurrentSpeed()
    { currentSpeed = shipRigBody.velocity.magnitude; }

    public void LeaderVelocity()
    { leaderVelocity = currentLeader.pilot.shipRigBody.velocity; }

    #endregion

    //  //
    #region Updates
    //  //

    //  track previous position
    protected Vector3 currentPosition;
    protected void FixedUpdate()
    {
        if (previousPosition != currentPosition)
        { previousPosition = currentPosition; }

        currentPosition = shipTrans.position;

        if (aiState.CurrentIsActive())
        {
            aiState.Update();
        }
        ship.AIFixedUpdate();
    }

    //  every 0.1 seconds
    public override void SlowUpdate()
    {
        if (aiState.CurrentIsActive())
        {
            aiState.SlowUpdate();
        }
        ship.SlowerUpdate();
    }

    //  every 0.5 seconds
    public override void SlowerUpdate()
    {
        if (aiState.CurrentIsActive())
        {
            aiState.SlowerUpdate();
        }

        DebugAI(aiState.queue, debugBehaviourList); //DEBUG
        ship.SlowerUpdate();
    }

    #endregion

    //  //
    #region Commands
    //  //

    //  follow
    public override void FollowLeader(bool now = false)
    {
        aiState.QueueFirst(new AI.Ship.Behaviour.Move.FollowLeader(this, aiState));
    }
    
    //  align
    public override void Align(SystmObject target)
    {
        aiState.QueueFirst(new AI.Ship.Behaviour.Move.Align(this, aiState, target));
    }

    //  stop
    public override void Stop(bool stop = true)
    {
        aiState.Clear();
        if (stop)
        {
            aiState.QueueFirst(new AI.Ship.Move.Stop(this, aiState));
        }
    }

    //  land
    public override void Land(Landable landable)
    {
        aiState.QueueFirst(new AI.Ship.Behaviour.Move.Land(this, aiState, landable));
    }

    public override void LandComplete(Landable landable)
    {
        if (autopilot)
        {
            autopilotPilot.LandComplete(landable);
        }
    }

    //  command
    public override void IssueCommand(ShipBehaviour command, bool now)
    {
        if (now)
        { aiState.QueueFirst(command); }
        else
        { aiState.Queue(command); }
    }

    //  update target
    public override void UpdateTarget(SystmObject target, bool dead)
    {
        base.UpdateTarget(target, dead);
        if (dead)
        {
            aiState.CycleStates();
        }
    }

    #endregion

    

}
