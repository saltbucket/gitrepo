﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AI
{
    public class Blank : ShipBehaviour
    {
        public Blank(AIShipController cntrllr, AIStateMachine thsMchn, string dWth = "kill", List<AIStateMachine> othrMchns = null)
        {
            ShipBehaviourConstructor(cntrllr, thsMchn, dWth, othrMchns);
        }
    }

    public static class Ship
    {
        public static class Behaviour
        {   
            public static class Move
            {
                /// follow another object
                public class FollowLeader : ShipBehaviour 
                {
                    /// <summary>
                    /// queues follow object if far away
                    /// queue match direction then match speed when close
                    /// </summary>

                    AIStateMachine mover;
                    Ship.Move.FollowObject follow;
                    Ship.Move.MatchDirection matchDirection;
                    Ship.Move.MatchSpeed matchSpeed;
                    GameObject leader;

                    float distance;

                    public FollowLeader(AIShipController cntrllr, AIStateMachine thsMchn, string dWth = "kill", List<AIStateMachine> othrMchns = null)
                    {
                        name = "Follow Leader";
                        ShipBehaviourConstructor(cntrllr, thsMchn, dWth, othrMchns);
                    }

                    public override void Enter()
                    {
                        con.currentLeader = con.Leader();
                        leader = con.currentLeader.gameObject;
                        
                        distance = 4;

                        mover = new AIStateMachine();

                        follow = new Ship.Move.FollowObject(con, mover, leader, cmpltWhn: distance);
                        matchDirection = new Ship.Move.MatchDirection(con, mover);
                        matchSpeed = new Ship.Move.MatchSpeed(con, mover, leader);
                        active = true;
                    }

                    public override void Update()
                    {
                        con.LeaderVelocity();
                       
                        con.DirectionToPoint(leader.transform.position);
                        con.AngleToPoint(leader.transform.position);
                        con.CurrentSpeed();
                                                
                        mover.Update();

                        //  face leader direction
                        if (mover.Idle())
                        {
                            con.FaceDirection(leader.transform.rotation);
                        }
                    }

                    public override void SlowUpdate()
                    {
                        con.DistanceToPoint(leader.transform.position);
                        if (!mover.IsCurrent(follow))
                        {
                            //  move to if far away
                            if (con.distanceToPoint >= distance && !mover.IsCurrent(follow))
                            {
                                mover.Clear();
                                mover.Queue(follow);
                            }
                            
                            //  match direction if different
                            if (!mover.queue.Contains(matchDirection) && !SystmAPI.Geometry.DirectionsMatch(con.shipRigBody.velocity, con.leaderVelocity, .4f) && con.currentSpeed > 1f)
                            {
                                mover.Queue(matchDirection);
                            }
                            
                            //  match speed if different
                            if (!SystmAPI.Generic.InRange(con.leaderVelocity.magnitude, con.currentSpeed, 1f) && !mover.queue.Contains(matchSpeed))
                            {
                                mover.Queue(matchSpeed);
                            }
                        }
                        
                        mover.SlowUpdate();
                    }

                    public override void Exit()
                    {
                        active = false;
                    }
                }

                /// moves to and stops at a fixed point
                public class Land : ShipBehaviour
                {
                    /// <summary>
                    /// queues stop, move at then stop again
                    /// completes when next to target and stopped
                    /// </summary>
                    AIStateMachine mover;
                    Landable landable;
                    bool directionChanged;

                    Ship.Move.ChangeDirectionToObject changeDirection;

                    public Land(AIShipController cntrllr, AIStateMachine thsMchn, Landable lndable, PlayerShipController player = null, string dWth = "kill", List<AIStateMachine> othrMchns = null)
                    {
                        base.ShipBehaviourConstructor(cntrllr, thsMchn, dWth, othrMchns);
                        landable = lndable;
                    }

                    public override void Enter()
                    {
                        mover = new AIStateMachine();

                        changeDirection = new AI.Ship.Move.ChangeDirectionToObject(con, mover, new GameObject[] { landable.gameObject, con.gameObject });
                        mover.Queue(changeDirection);

                        active = true;
                    }
                    public override void Update()
                    {
                        con.DebugAI(mover.queue, con.debugSubBehaviourList); //DEBUG

                        con.DistanceToPoint(landable.transform.position);
                        con.CurrentSpeed();

                        if (con.distanceToPoint < 5f && con.currentSpeed < 0.01f)
                        {
                            con.LandComplete(landable);
                            StateComplete();
                            return;
                        }

                        mover.Update();
                    }

                    public override void SlowUpdate()
                    {
                        if (!directionChanged && SystmAPI.Geometry.DirectionsMatch(con.shipRigBody.velocity, landable.transform.position - con.transform.position, 0.025f))
                        {
                            directionChanged = true;
                            mover.Queue(new Ship.Move.To(con, mover, landable.transform.position));
                            mover.Queue(new Ship.Move.Stop(con, mover));
                        }

                        mover.SlowUpdate();
                    }

                    public override void Exit()
                    {
                        active = false;
                    }
                }

                /// matches the velocity of and aligns ship with the target object
                public class Align : ShipBehaviour
                {
                    /// <summary>
                    /// moves and matches velocity within close range
                    /// adjusts distance to align with target
                    /// </summary>
                    AIStateMachine mover;
                    SystmObject target;
                    bool stopping;
                    
                    public Align(AIShipController cntrllr, AIStateMachine thsMchn, SystmObject trget, string dWth = "kill", List<AIStateMachine> othrMchns = null)
                    {
                        ShipBehaviourConstructor(cntrllr, thsMchn, dWth, othrMchns);
                        target = trget;
                        con.currentLeader = target;
                    }

                    public override void Enter()
                    {
                        stopping = false;
                        mover = new AIStateMachine();
                        mover.Queue(new Ship.Move.At(con, mover, target.gameObject));

                        if (target.pilot.Moving())
                        {
                            mover.Queue(new Ship.Move.MatchDirection(con, mover));
                            mover.Queue(new Ship.Move.MatchSpeed(con, mover, target.gameObject));
                            mover.Queue(new Ship.Move.ForceMatchVelocity(con, mover, target.pilot.shipRigBody.velocity));
                            mover.Queue(new Ship.Move.CorrectPosition(con, mover, target));
                        }
                        else
                        {
                            mover.Queue(new Ship.Move.Stop(con, mover));
                        }

                        active = true;
                    }

                    public override void Update()
                    {
                        con.DirectionToPoint(target.transform.position);
                        con.AngleToPoint(target.transform.position);
                        con.DistanceToPoint(target.transform.position);
                        con.CurrentSpeed();

                        if (stopping)
                        {
                            //  state 'finished'
                            if (mover.Idle())
                            {
                                //  check if successfully aligned, repeat state if not
                                if (con.distanceToPoint > 0.1f)
                                {
                                    doWith = "next";
                                }
                                else
                                {
                                    doWith = "kill";
                                    con.shipRigBody.velocity = target.pilot.shipRigBody.velocity;
                                }
                                StateComplete();
                            }
                            con.LeaderVelocity();
                        }

                        //  calculate stop distance, stop at that distance
                        float stopDistance = SystmAPI.Physics.GetStopDistance(con, con.currentSpeed - con.leaderVelocity.magnitude);
                        if (stopDistance >= con.distanceToPoint && !stopping)
                        {
                           
                            stopping = true;


                            mover.CycleStates();
                        }

                        mover.Update();
                    }

                    public override void SlowUpdate()
                    {
                        mover.SlowUpdate();
                    }

                    public override void SlowerUpdate()//DEBUG
                    {
                        //con.DebugAI(mover.queue, con.debugSubBehaviourList);
                    }//DEBUG

                    public override void Exit()
                    {
                        active = false;
                    }
                }
            }
            
            public static class Attack
            {
                /// swooping attack
                public class Swoop : ShipBehaviour
                {
                    /// <summary>
                    /// queues and loops follow target, move forwad then stop
                    /// fires weapons whenever the target is in range
                    /// </summary>
                    SystmObject target;
                    AIStateMachine stateMachine;

                    AIStateMachine mover;
                    AIStateMachine shooter;

                    public Swoop(AIShipController cntroller, AIStateMachine sMachine, SystmObject trgt, string dWith = "kill", List<AIStateMachine> othrMchns = null)
                    {
                        ShipBehaviourConstructor(cntroller, sMachine, dWith, othrMchns);
                        stateMachine = sMachine;
                        target = trgt;
                    }

                    public override void Enter()
                    {
                        Debug.Log("running enter in " + con.name);
                        if (target == null)
                        {
                            StateComplete();
                            return;
                        }
                        else
                        {
                            con.NewTarget(target);
                        }

                        //  state machines
                        mover = new AIStateMachine();
                        shooter = new AIStateMachine();

                        //  states
                        mover.Queue(new Ship.Move.FollowTarget(con, mover, cmpltWhn: 2, dWth: "queue"));
                        mover.Queue(new Ship.Move.Forward(con, mover, cmpltWhn: 3, dWth: "queue"));
                        mover.Queue(new Ship.Move.Stop(con, mover, cmpltWhn: 2, dWth: "queue"));
                        shooter.Queue(new Ship.Attack.Cannons(con, shooter));
                        active = true;
                    }

                    public override void Update()
                    {
                        con.DirectionToTarget();
                        con.AngleToTarget();
                        con.CurrentSpeed();

                        mover.Update();
                    }

                    public override void SlowUpdate()
                    {
                        con.DistanceToTarget();
                        mover.SlowUpdate();
                        shooter.SlowUpdate();
                    }

                    public override void Exit()
                    {
                        if (!active)
                        {
                            return;
                        }
                        else
                        {
                            active = false;
                            shooter.Clear();
                            con.DropTarget();
                        }
                        
                    }
                }
            }
        }


        public static class Move
        {
            //  //
            #region Match Movement
            //  //

            ///  set velocity to match given value then exit
            public class ForceMatchVelocity : ShipBehaviour
            {
                Vector2 velocity;
                public ForceMatchVelocity(AIShipController cntrllr, AIStateMachine thsMchn, Vector2 vlocity, string dWth = "kill", List<AIStateMachine> othrMchns = null)
                {
                    ShipBehaviourConstructor(cntrllr, thsMchn, dWth, othrMchns);
                    velocity = vlocity;
                }
                public override void Enter()
                {
                    con.shipRigBody.velocity = velocity;
                    StateComplete();
                }
            }

            ///  accellerate towards target, decelerate when close
            public class CorrectPosition : ShipBehaviour
            {
                /// <summary>
                /// Requires:
                ///     DistanceToPoint()
                ///     DirectionToPoint()
                /// </summary>

                SystmObject target;
                float threshold;
                bool moving;
                bool stopping;
                int count = 0;
                float max;
                Quaternion directionFromPoint;
                Vector2 originalVelocity;

                public CorrectPosition(AIShipController cntrllr, AIStateMachine thsMchn, SystmObject trget, float thrshold = 0.1f, string dWth = "kill", List<AIStateMachine> othrMchns = null)
                {
                    ShipBehaviourConstructor(cntrllr, thsMchn, dWth, othrMchns);
                    target = trget;
                    threshold = thrshold;
                }

                public override void Enter()
                {
                    moving = false;
                    stopping = false;
                    directionFromPoint = SystmAPI.Geometry.GetDirection(con.shipTrans.position, target.transform.position);
                    originalVelocity = target.pilot.shipRigBody.velocity;
                    max = 5 * con.distanceToPoint;


                    if (con.distanceToPoint <= threshold)
                    {
                        StateComplete();
                    }
                    else
                    {
                        active = true;
                    }
                }

                public override void Update()
                {
                    //  start accel
                    if (!moving && con.FaceDirection(con.directionToPoint))
                    {
                        con.Accelerate();
                        count += 1;
                    }

                    //  stop accel
                    if (count > max)
                    {
                        if (!moving)
                        {
                            moving = true;
                            return;
                        }

                        if (stopping)
                        {
                            StateComplete();
                            return;
                        }

                        if (!stopping && con.FaceDirection(directionFromPoint) && con.distanceToPoint <= threshold)
                        {
                            count = 0;
                            stopping = true;
                            return;
                        }

                    }
                    else if (stopping)
                    {
                        con.Accelerate();
                        count += 1;
                    }

                    //  start decel  
                }

                public override void SlowUpdate()
                {
                    //  if target moved away or was missed, abandon state
                    if (target.pilot.shipRigBody.velocity != originalVelocity || con.MovingAway(target.transform.position))
                    {
                        StateComplete();
                    }
                }
                public override void Exit()
                {
                    active = false;
                }
            }

            /// accelerate or decelerate to match target speed 
            public class MatchSpeed : ShipBehaviour
            {
                /// <summary>
                /// Requires:
                ///     LeaderVelocity()
                /// </summary>

                GameObject leader;

                Quaternion backwards;

                public MatchSpeed(AIShipController cntrllr, AIStateMachine thsMchn, GameObject ldr, string dWth = "kill", List<AIStateMachine> othrMchns = null)
                {
                    ShipBehaviourConstructor(cntrllr, thsMchn, dWth, othrMchns);
                    leader = ldr;
                }
                
                public override void Enter()
                {
                    backwards = SystmAPI.Geometry.Backwards(con.shipRigBody.velocity);
                }

                public override void Update()
                {
                    float speed = con.shipRigBody.velocity.magnitude;
                    
                    if (SystmAPI.Generic.InRange(speed, con.leaderVelocity.magnitude, 0.1f))
                    {
                        con.Thrust(false);
                        StateComplete();
                        
                        return;
                    }

                    if (speed > con.leaderVelocity.magnitude && con.FaceDirection(backwards))
                    { con.Accelerate(); }
                    else if (speed < con.leaderVelocity.magnitude && con.FaceDirection(leader.transform.rotation))
                    { con.Accelerate(); }
                    else
                    { con.Thrust(false); }
                }

                public override void Exit()
                { con.Thrust(false); }
            }

            /// matches the direction of movement by correcting using forward thrust
            public class MatchDirection : ShipBehaviour
            {
                /// <summary>
                ///     CurrentSpeed()
                ///     leaderVelocity
                /// </summary>
                Quaternion correctiveDirection;
                
                public MatchDirection(AIShipController cntrllr, AIStateMachine thsMchn, string dWth = "kill", List<AIStateMachine> othrMchns = null)
                {
                    ShipBehaviourConstructor(cntrllr, thsMchn, dWth, othrMchns);
                }

                public override void Enter()
                { correctiveDirection = SystmAPI.Geometry.GetCorrectiveDirection(con.shipRigBody.velocity, con.leaderVelocity); }

                public override void Update()
                {
                    if (con.currentSpeed < 0.1f)
                    {
                        StateComplete();
                        return;
                    }

                    correctiveDirection = SystmAPI.Geometry.GetCorrectiveDirection(con.shipRigBody.velocity, con.leaderVelocity);
                    if (con.FaceDirection(correctiveDirection))
                    {
                        if (!SystmAPI.Geometry.DirectionsMatch(con.shipRigBody.velocity, con.leaderVelocity, 0.1f))
                        { con.Accelerate(); }
                        else
                        { StateComplete(); }
                    }
                }

                public override void Exit()
                { con.Thrust(false); }
            }

            public class ChangeDirectionToObject : ShipBehaviour
            {
                /// <summary>
                ///     CurrentSpeed()
                /// </summary>
                Quaternion correctiveDirection;
                GameObject[] objects;

                public ChangeDirectionToObject(AIShipController cntrllr, AIStateMachine thsMchn, GameObject[] bjects, string dWth = "kill", List<AIStateMachine> othrMchns = null)
                {
                    ShipBehaviourConstructor(cntrllr, thsMchn, dWth, othrMchns);
                    objects = bjects;
                }

                public override void Enter()
                {
                    correctiveDirection = SystmAPI.Geometry.GetCorrectiveDirection(con.shipRigBody.velocity, objects[0].transform.position - objects[1].transform.position);
                }

                public override void Update()
                {
                    if (con.currentSpeed < 0.1f)
                    {
                        StateComplete();
                        return;
                    }

                    Vector2 directionTo = objects[0].transform.position - objects[1].transform.position;
                    correctiveDirection = SystmAPI.Geometry.GetCorrectiveDirection(con.shipRigBody.velocity, directionTo);
                    if (con.FaceDirection(correctiveDirection))
                    {
                        if (!SystmAPI.Geometry.DirectionsMatch(con.shipRigBody.velocity, directionTo, 0.025f))
                        { con.Accelerate(); }
                        else
                        {
                            StateComplete();
                        }
                    }
                }

                public override void Exit()
                { con.Thrust(false); }
            }

            #endregion

            //  //
            #region Follow
            //  //

            /// follow target object until given distance or stop distance
            public class FollowObject : ShipBehaviour
            {
                /// <summary>
                /// Requires:
                ///     Update: DirectionToPoint, CurrentSpeed, AngleToPoint
                ///     SlowUpdate: DistanceToPoint
                /// </summary>

                public float maxSpeed;
                float completeWhen;
                GameObject gameObject;

                float objectSpeed;
                Vector2 previousPosition;

                public FollowObject(AIShipController cntrllr, AIStateMachine thsMchn, GameObject gmObjct, float mxSpd = 0, float cmpltWhn = -1, string dWth = "kill", List<AIStateMachine> othrMchns = null)
                {
                    ShipBehaviourConstructor(cntrllr, thsMchn, dWth, othrMchns);

                    completeWhen = cmpltWhn;
                    gameObject = gmObjct;

                    if (mxSpd == 0)
                    {
                        maxSpeed = con.ship.speed;
                    }
                    else
                    {

                        maxSpeed = mxSpd;
                    }
                    active = true;
                }

                private void GetObjectSpeed()
                {
                    objectSpeed = ((Vector2)gameObject.transform.position - previousPosition).magnitude / Time.deltaTime;
                    previousPosition = gameObject.transform.position;
                }

                public override void Update()
                {
                    GetObjectSpeed();
                    con.FaceDirection(con.directionToPoint);
                    if (con.angleToPoint < 25)
                    {
                        if (con.currentSpeed <= maxSpeed || con.MovingAway(gameObject.transform.position))
                        {
                            con.Accelerate();
                        }
                        else
                        {
                            con.Thrust(false);
                        }
                    }
                }

                public override void SlowUpdate()
                {
                    if (completeWhen != -1 && con.distanceToPoint <= completeWhen)
                    {
                        StateComplete();
                    }
                    else if (completeWhen == -1)
                    {
                        float stopDistance = SystmAPI.Physics.GetStopDistance(con, con.currentSpeed - objectSpeed);
                        if (con.distanceToPoint <= stopDistance)
                        {
                            Debug.Log(con.distanceToPoint + " <= " + stopDistance);
                            StateComplete();
                        }
                    }
                    con.DebugAI(thisMachine.queue, con.debugSubBehaviourList); //DEBUG
                }
                
                public override void Exit()
                {
                    con.Thrust(false);
                    active = false;
                }
            }

            /// follow locked target until given distance or stop distance
            public class FollowTarget : ShipBehaviour
            {
                /// <summary>
                /// Requires:
                ///     Update: DirectionToTarget(), CurrentSpeed(), AngleToTarget()
                ///     SlowUpdate: DistanceToTarget()
                /// </summary>
                /// 

                float maxSpeed;
                float completeWhen;

                public FollowTarget(AIShipController cntrllr, AIStateMachine thsMchn, float mxSpd = 0, float cmpltWhn = -1, string dWth = "kill", List<AIStateMachine> othrMchns = null)
                {
                    ShipBehaviourConstructor(cntrllr, thsMchn, dWth, othrMchns);

                    completeWhen = cmpltWhn;

                    if (mxSpd == 0)
                    {
                        maxSpeed = con.ship.speed;
                    }
                    else
                    {
                        maxSpeed = mxSpd;
                    }
                }

                public override void Update()
                {
                    con.FaceDirection(con.directionToTarget);
                    if (con.angleToTarget < 25)
                    {
                        if (con.currentSpeed < maxSpeed)
                        {
                            con.Accelerate();
                        }
                        else
                        {
                            con.Thrust(false);
                        }
                    }
                }

                public override void SlowUpdate()
                {
                    if (completeWhen != -1 && con.distanceToTarget <= completeWhen)
                    {
                        StateComplete();
                    }
                }
            }

            #endregion

            //  //
            #region Basic
                    //  //

            /// move forward a distance
            public class Forward : ShipBehaviour
            {
                float completeWhen;
                Vector2 startPosition;
                
                public Forward(AIShipController cntrllr, AIStateMachine thsMchn, float cmpltWhn, string dWth = "kill", List<AIStateMachine> othrMchns = null)
                {
                    ShipBehaviourConstructor(cntrllr, thsMchn, dWth, othrMchns);
                    completeWhen = cmpltWhn;
                }

                public override void Enter()
                {
                    startPosition = con.transform.position;
                }

                public override void Update()
                {
                    if (Vector2.Distance(startPosition, con.transform.position) >= completeWhen)
                    {
                        StateComplete();
                    }
                    else
                    {
                        con.Accelerate();
                    }
                }

                public override void Exit()
                {
                    con.Thrust(false);
                }
            }

            /// move at a point until distance - queue stop next
            public class To : ShipBehaviour
            {
                Vector2 targetPosition;
                float completeWhen;

                public To(AIShipController cntrllr, AIStateMachine thsMchn, Vector2 trgetPosition, string dWth = "kill", List<AIStateMachine> othrMchns = null)
                {
                    ShipBehaviourConstructor(cntrllr, thsMchn, dWth, othrMchns);
                    targetPosition = trgetPosition;
                }

                public override void Enter()
                {
                    active = true;
                }

                public override void Update()
                {
                    completeWhen = SystmAPI.Physics.GetStopDistance(con, con.currentSpeed);
                    if (con.distanceToPoint <= completeWhen)
                    {
                        active = false;
                        StateComplete();
                        return;
                    }
                    else if (con.FaceDirection(SystmAPI.Geometry.GetDirection(targetPosition, con.transform.position)))
                    {
                        con.Accelerate();
                    }
                }
            }

            
            /// move at an object
            public class At : ShipBehaviour
            {
                GameObject target;

                //  constructor
                public At(AIShipController cntrllr, AIStateMachine thsMchn, GameObject trget, string dWth = "kill", List<AIStateMachine> othrMchns = null)
                {
                    ShipBehaviourConstructor(cntrllr, thsMchn, dWth, othrMchns);
                    target = trget;
                }

                public override void Enter()
                {
                    active = true;
                }

                //  accelerate if facing - face target
                public override void Update()
                {
                    if (con.angleToPoint < 1)
                    {
                        con.Accelerate(con.ship.speed);
                    }

                    con.FaceDirection(con.directionToPoint);
                }

                public override void Exit()
                {
                    con.Thrust(false);
                    active = false;
                }
            }

            /// stop moving
            public class Stop : ShipBehaviour
            {
                /// <summary>
                /// Requires:
                ///     Update: CurrentSpeed
                /// </summary>
                
                public float completeWhen;
                //constructor
                public Stop(AIShipController cntrllr, AIStateMachine thsMchn, float cmpltWhn = 0, string dWth = "kill", List<AIStateMachine> othrMchns = null)
                {
                    ShipBehaviourConstructor(cntrllr, thsMchn, dWth, othrMchns);
                    completeWhen = cmpltWhn;
                }

                //  AI can't quite slow to 0
                public override void Enter()
                {
                    if (completeWhen < 0.02f)
                    {
                        completeWhen = 0.02f;
                    }
                    active = true;
                }
                public override void Update()
                {
                    //  stop ship if almost stopped
                    if (con.currentSpeed < completeWhen && completeWhen == 0.02f)
                    {
                        con.shipRigBody.velocity = new Vector2();
                    }

                    //  face backwards then thrust
                    if (con.currentSpeed >= completeWhen)
                    {
                        if (con.FaceBackwards())
                        {
                            con.Accelerate(con.ship.speed);
                        }

                    }
                    else
                    {
                        con.Thrust(false);
                    }
                }

                //  next
                public override void SlowUpdate()
                {
                    if (thisMachine.pilot)
                    {
                        con.CurrentSpeed();
                    }
                    if (con.currentSpeed <= completeWhen)
                    {
                        StateComplete();
                    }
                }
                public override void Exit()
                {
                    active = false;
                }
            }

            #endregion
        }

        public static class Attack
        {
            /// fire cannons when in range of target
            public class Cannons : ShipBehaviour
            {
                /// <summary>
                /// Requires:
                ///     SlowUpdate: AngleToTarget, DistanceToTarget
                /// </summary>
                float angle = 3;
                float distance = 10;

                public Cannons(AIShipController cntrllr, AIStateMachine thsMchn, string dWth = "kill", List<AIStateMachine> othrMchns = null)
                {
                    ShipBehaviourConstructor(cntrllr, thsMchn, dWth, othrMchns);
                }

                public override void SlowUpdate()
                {
                    if (con.firingPrimary && (con.angleToTarget >= angle || con.distanceToTarget > distance))
                    {
                        con.FirePrimaryUp();
                    }
                    else if (!con.firingPrimary && (con.angleToTarget <= angle && con.distanceToTarget < distance))
                    {
                        con.FirePrimaryDown();
                    }
                }
                public override void Exit()
                {
                    con.FirePrimaryUp();
                }
            }
        }
    }
}
