﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandGroup
{    
    //public SystmObject commander;
    /*public SystmObject leader;
    
    public int number;

    public List<SystmObject> members = new List<SystmObject>();
    public Dictionary<SystmObject, AIShipController> memberAIPilots = new Dictionary<SystmObject, AIShipController>();

    public List<SystmObject> targets = new List<SystmObject>();
    
    //  //
    #region Members
            //  //

    //  change leader
    public void NewLeader(SystmObject newLeader)
    {
        if (leader != null)
        {
            leader.isLeader = null;
        }
        newLeader.isLeader = this;
        leader = newLeader;
    }

    //  new member
    public void AddToGroup(SystmObject member)
    {
        if (member == null)
        {
            Debug.Log("tried to add member " + member);
        }
        members.Add(member);
        memberAIPilots[member] = member.GetComponentInChildren<AIShipController>();
        member.isMember = this;
    }

    //  drop member
    public void RemoveFromGroup(SystmObject member)
    {
        if (!members.Contains(member))
        {
            Debug.Log(member + " is not in group " + number + "!");
            return;
        }
        members.Remove(member);
        memberAIPilots.Remove(member);
        member.isMember = null;
    }

    #endregion

    //  //
    #region Commands
    //  //

    //  attack
    public void GroupAttackTarget(SystmObject target)
    {
        foreach (SystmObject member in members)
        {
            if (member.pilot.tag != "Player")
            {
                memberAIPilots[member].AttackTarget(target, true);
            }
        }
    }

    //  follow
    public void GroupFollow()
    {
        foreach (SystmObject member in members)
        {
            if (member.pilot.tag != "Player")
            {
                memberAIPilots[member].IssueCommand(new AI.Ship.Behaviour.Move.FollowLeader(memberAIPilots[member], memberAIPilots[member].aiState, leader.gameObject), true);
            }
        }
    }

    #endregion*/
}
