﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AIStateMachine
{
    AIShipController controller;
    public bool pilot;
    public bool defaultBehaviour;
    public AIStateMachine(AIShipController con = null, bool plot = false)
    {
        controller = con;
        pilot = plot;
    }
    public List<ShipBehaviour> queue = new List<ShipBehaviour>();

    // run next state in list
    public void CycleStates()
    {
        if (queue.Count == 0)
        {
            return;
        }

        Exit(0);
        ShipBehaviour completedState = queue[0];
        queue.RemoveAt(0);

        OnComplete(completedState);

        if (queue.Count == 0 && pilot)
        {
            defaultBehaviour = true;
            controller.DefaultBehaviour();
            return;
        }
        else
        {
            defaultBehaviour = false;
            Enter(0);
        }
    }

    //  discard, queue or insert completed state
    private void OnComplete(ShipBehaviour completedState)
    {
        switch (completedState.DoWith())
        {
            case "kill":
                break;

            case "queue":
                queue.Add(completedState);
                break;

            case "next":
                queue.Insert(0, completedState);
                break;

            default:
                Debug.Log("NextState " + completedState.DoWith() + " not a valid option");
                break;
        }
    }

    //  enter new state
    private void Enter(int index)
    {
        if (queue.Count == 0)
        {
            return;
        }
        queue[index].Enter();
        //Debug.Log("Entering " + queue[index]);
    }

    //  exit old state
    private void Exit(int index)
    {
        queue[index].Exit();
        //Debug.Log("Exiting " + queue[index]);
    }

    //  add to queue and run Enter() if queue was empty
    public void Queue(ShipBehaviour state)
    {
        //Debug.Log("adding " + state);
        queue.Add(state);
        if (queue.Count == 1)
        {
            Enter(0);
        }
    }

    //  insert at the start of the queue and run Enter()
    public void QueueFirst(ShipBehaviour state)
    {
        if (queue.Count > 0)
        {
            Exit(0);
        }
        queue.Insert(0, state);
        Enter(0); 
    }

    //  clear queue
    public void Clear()
    {
        if (queue.Count > 0)
        {
            Exit(0);
            queue.Clear();
        }
    }

    //  get current state
    public ShipBehaviour Current()
    {
        if (!Idle())
        {
            return (ShipBehaviour) queue[0];
        }
        else
        {
            return null;
        }
    }

    //  check if current state has set active to true
    public bool CurrentIsActive()
    {
        if (!Idle() && Current().active)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    //  is current behaviour (specific instance not type)
    public bool IsCurrent(ShipBehaviour current)
    {
        if (queue.Count > 0 && queue[0] == current)
        { return true; }
        else
        { return false; }
    }

    //  state is in queue (specific instance not type)
    public bool Contains(ShipBehaviour behaviour)
    {
        if (queue.Contains(behaviour))
        { return true; }
        else
        { return false; }
    }

    //  queue is empty
    public bool Idle()
    {
        if (queue.Count == 0)
        { return true; }
        else
        { return false; }
    }
   
    //  on FixedUpdate
    public void Update()
    {
        if (queue.Count > 0)
        { queue[0].Update(); }
    }

    //  10/sec
    public void SlowUpdate()
    {
        if (queue.Count > 0)
        { queue[0].SlowUpdate(); }
    }

    //  2/sec
    public void SlowerUpdate()
    {
        if (queue.Count > 0)
        { queue[0].SlowerUpdate(); }
    }
}
