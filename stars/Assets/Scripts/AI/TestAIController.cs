﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//  AI controller for specific pilot types
public class TestAIController : AIShipController
{
    //  //
    #region Commands
    //  //

    //  runs whenever queue is empty
    public override void DefaultBehaviour()
    {
        FollowLeader(true);
    }

    //  attack target
    public override void Attack(SystmObject target, bool now = false)
    {
        if (now)
        {
            aiState.QueueFirst(new AI.Ship.Behaviour.Attack.Swoop(this, aiState, target));
        }
        else
        {
            aiState.Queue(new AI.Ship.Behaviour.Attack.Swoop(this, aiState, target));
        }
    }
    
    #endregion

    //  //
    #region Targeting
    //  //

    //  other object has targeted this
    public override void TargetedBy(SystmObject targeter, bool targeted)
    {
        //  TODO:
        //  this needs to decide whether or not to prioritise a second attacker, if already in combat
        //  some of this code should probably be moved to base class - ShipAIController (?exit derived method from base method?)

        if (aiState.Idle() || aiState.defaultBehaviour)
        {
            Attack(targeter, true);
        }
        NewEnemy(targeter.pilot);
    }

    #endregion
}
