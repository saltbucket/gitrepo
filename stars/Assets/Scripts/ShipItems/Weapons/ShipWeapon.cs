﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

//ship weapon stats and behaviour 

public class ShipWeapon : Item
{
    //string weapUniqueName;
    [Header("Weapon")]
    protected bool firing = false;
    [SerializeField]

    public GameObject[] weapProj;

    public int projSelect;
    public float fireRate;
    protected GameObject prefab;
    
    [HideInInspector]
    public SystmObject systmObject;
    [HideInInspector]
    public SystmObject.ShipWeapSlot slot;
    [HideInInspector]
    public Rigidbody2D shipRigidBody;
    [HideInInspector]
    public Projectile currentProjectile;
    
    public void Assign(SystmObject sObject, SystmObject.ShipWeapSlot slt)
    {
        systmObject = sObject;
        shipRigidBody = sObject.pilot.shipRigBody;
        slot = slt;
        RefreshProjectileType();
    }

    //  fire
    public virtual void FireDown() { }
    public virtual void FireUp() { }

    //  refresh selected projectile type
    protected void RefreshProjectileType()
    {
        //  get current projectile type, add to storage lists
        prefab = weapProj[projSelect - 1];

        currentProjectile = weapProj[projSelect - 1].GetComponent<Projectile>();

        DumpStoredProj();
        projActive = new List<GameObject>();
    }

    #region Recycling

    //  active and inactive projectile instances
    protected List<GameObject> projStore = new List<GameObject>();
    protected List<GameObject> projActive = new List<GameObject>();

    //  reset projectile location to weapon
    private void Reposition(Transform trans)
    {
        trans.position = transform.position;
        trans.rotation = transform.rotation;
    }

    //  return a new/unused projectile for use
    protected GameObject NewProj(GameObject proj)
    {
        if (projStore.Count == 0)
        {
            //  none stored, create projectile
            proj = Instantiate(proj);                                                       
            projActive.Add(proj);

            Reposition(proj.transform);
            return proj;
        }
        else
        {
            //  assign to proj, remove from store
            proj = projStore[0];
            projStore.Remove(proj);

            Reposition(proj.transform);
            return proj;
        }
    }

    //  store/delete used projectiles
    public void Store(GameObject proj)
    {
        //if too many projectiles destroy, else store
        if (projStore.Count > systmObject.weapSlots_Primary.Length * 10 || !firing)
        {
            //Debug.Log("destroyed");
            Destroy(proj);
        }
            
        else
        {
            //Debug.Log("stored");
            Reposition(proj.transform);
            projStore.Add(proj);
        }
    }

    //  dump all projectiles
    public void DumpStoredProj()
    {
        foreach (GameObject gObj in projStore)
        {
            Destroy(gObj);
        }
        projStore = new List<GameObject>();
    }
    #endregion


}
