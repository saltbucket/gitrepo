﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : Item
{
    [Header(":: Projectile")]
    public float speed;
    public float expire;
    public int projDamage;
    public bool infinite;

    protected Rigidbody2D projRB;
    
    protected ShipWeapon weapon;

    // assign rigidbody
    private void Awake()
    {
        projRB = gameObject.GetComponent<Rigidbody2D>();
    }

    // fire projectile
    public virtual void Go(ShipWeapon wpn, Rigidbody2D shipRB) { }

    // store projectile at source weapon
    protected virtual void DestroyProj()
    {
        projRB.velocity = new Vector2();
        gameObject.SetActive(false);
        if (weapon != null)
        {
            weapon.Store(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    //  on collision
    protected void Hit(SystmObject objectHit, Projectile proj)
    {
        if (objectHit.GetInstanceID() != weapon.systmObject.GetInstanceID())
        {
            objectHit.Hit(weapon.systmObject, proj);
            DestroyProj();
        }     
    }
}