﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserProj : Projectile
{
    //  activate projectile, apply velocity
    public override void Go(ShipWeapon wpn, Rigidbody2D shipRB)
    {
        weapon = wpn;
        gameObject.SetActive(true);
        projRB.velocity = (Vector2)transform.up * speed;
        StartCoroutine(Wait(expire));       
    }

    //  wait before destroying object
    IEnumerator Wait(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        //  after waiting, deactivate and store
        if (gameObject.activeInHierarchy == true)
        {
            base.DestroyProj();
        }
    }

    //  hit SystmObject
    private void OnTriggerEnter2D(Collider2D other)
    {
        SystmObject objectHit = other.GetComponent<SystmObject>();

        //WEAPON IMPACT EFFECTS HERE
        Hit(objectHit, this);
            
    }

}
