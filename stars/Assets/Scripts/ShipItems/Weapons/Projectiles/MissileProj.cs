﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileProj : Projectile
{
    [Header(":: Missile Projectile")]
    public float turn;
    public float accel;
    //public float maxSpeed;

    private float currentSpeed = 0;

    private bool ignited = false;
    public SystmObject target = null;
    private Vector2 shipVelocity;

    //  //
    #region Go
            //  //

    //  activate projectile, apply velocity
    public override void Go(ShipWeapon wpn, Rigidbody2D shipRB)//, GameObject target)
    {
        // source and target
        weapon = wpn;
        target = weapon.systmObject.target;

        // activate and start expiry timer
        gameObject.SetActive(true);
        StartCoroutine(WaitThenDestroy(expire));

        // release object from ship
        if (weapon.slot.onRightHandSide)
            projRB.velocity += (Vector2) (transform.right / 3);
        else
            projRB.velocity += (Vector2) (-transform.right / 3);

        // adjust projectile velocity for ship movement
        shipVelocity = shipRB.velocity;
        projRB.velocity += shipVelocity;

        // wait before starting persuit
        StartCoroutine(WaitThenIgnite(.5f));
    }
    
    private void FixedUpdate()
    {
        if (ignited)
        {
            if (currentSpeed < speed)
            {
                // gradual exceleration to max speed
                currentSpeed += accel;
                currentSpeed = Mathf.Clamp(currentSpeed, 0, speed);
            }
            if (target != null)
            {
                FaceDirection(SystmAPI.Geometry.GetDirection(target.transform.position, transform.position));
            }

            // move forward
            transform.position += transform.up * currentSpeed;      
        }
    }

    IEnumerator WaitThenIgnite(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        /*
        projRB.velocity -= shipVelocity;
        if (weapon.slot.onRightHandSide)
            projRB.velocity -= (Vector2)(transform.right / 3);
        else
            projRB.velocity -= (Vector2)(-transform.right / 3);*/

        projRB.velocity = new Vector2();

        ignited = true;
    }

    #endregion

    //  //
    #region Hit/Destroy
            //  //

    //  wait before destroying object
    IEnumerator WaitThenDestroy(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        //  after waiting, deactivate and store
        if (gameObject.activeInHierarchy == true)
        {
            DestroyProj();
        }
        StopAllCoroutines();
    }

    //  hit SystmObject
    private void OnTriggerEnter2D(Collider2D other)
    {
        SystmObject objectHit = other.GetComponent<SystmObject>();

        //WEAPON IMPACT EFFECTS HERE
        Hit(objectHit, this);
    }

    //  reset type-specific values ready for next firing
    protected override void DestroyProj()
    {
        base.DestroyProj();
        currentSpeed = 0;
        ignited = false;
    }

    #endregion

    //  //
    #region Navigation
    //  //

    public void FaceDirection(Quaternion dir)
    {
        //Rotation relative to desired direction
        Quaternion relativeToDir = Quaternion.Inverse(transform.rotation) * dir;
        float facing = relativeToDir.eulerAngles.z;

        float turnModifier = (turn * currentSpeed) * 10;

        if (!SystmAPI.Generic.InRange(facing, 0, turn))
        {
            if (facing >= 180)
                Right(turnModifier);
            else if (facing < 180)
                Left(turnModifier);
        }
        else
        {
            transform.rotation = dir;
        }
    }

    public void Left(float modifier) { transform.Rotate(Vector3.forward * modifier); }
    public void Right(float modifier) { transform.Rotate(-Vector3.forward * modifier); }

    #endregion


}
