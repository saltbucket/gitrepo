﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyWeapon : ShipWeapon
{
    //  start firing
    public override void FireDown()
    {
        firing = true;
        RefreshProjectileType();
        InvokeRepeating("Fire", 0f, fireRate);
    }

    //  stop firing
    public override void FireUp()
    {
        firing = false;
        CancelInvoke();
        DumpStoredProj();
    }

    //  fire a projectile
    private void Fire()
    {
        //  get new projectile
        GameObject proj;
        proj = NewProj(prefab);

        //  go
        Projectile behaviour = proj.GetComponent<Projectile>();
        behaviour.Go(this, shipRigidBody);
    }
}
