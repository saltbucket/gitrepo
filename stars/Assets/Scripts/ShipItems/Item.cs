﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

//global item stats and behaviour
public class Item : MonoBehaviour
{
    [Header("Item")]
    public string itemType;
    public Sprite image;
    public string itemName;
    public string value;
    public string tooltip;

    public string label;
    
    public int count;
    public int itemClass;
}
