﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Ship : SystmObject
{
    [Header(":: Ship")]

	public float thrust;
    public float turn;
    public int breaks;
    public float speed;

    public GameObject thrustObject;
}
