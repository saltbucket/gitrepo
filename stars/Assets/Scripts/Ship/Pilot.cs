﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pilot : MonoBehaviour
{

    [Header("Pilot")]    
    //  ship SystmObject components
    public Ship ship;
    [HideInInspector]
    public Transform shipTrans;
    [HideInInspector]
    public Rigidbody2D shipRigBody;

    public bool ai;
    public List<Pilot> enemies = new List<Pilot>();

    public bool firingPrimary = false;
    public bool firingSecondary = false;
    private bool thrustOn;
    
    [Header("Fleet")]
    public bool commander;
    public int numberOfGroups;
    public Fleet fleet;
    public int groupNumber;
    public Combat combat;

    [Header("AutoPilot")]
    public bool autopilot;
    //  the pilot that owns this autopilot
    public Pilot autopilotPilot;

    //  //
    #region Fleet
    //  //

    public bool InCombat()
    {
        if (combat == null)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    public bool FleetMember()
    {
        if (fleet == null)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    public SystmObject Commander()
    {
        return fleet.commander.ship;
    }
    public SystmObject Leader()
    {
        return fleet.commander.ship;/*
        if (fleet.leaders[groupNumber] == this)
        {
            return fleet.commander.ship;
        }
        else
        {
            return fleet.leaders[groupNumber].ship;
        }*/
    }

    public void CreateFleet()
    {
        fleet = new Fleet(this, numberOfGroups);
    }
    
    #endregion

    //  //
    #region Generic
    //  //

    public virtual void RefreshShip()
    {
        //  create fleet class
        if (commander && fleet == null)
        {
            CreateFleet();
        }

        GetShipComponents();

        //  give ship this as it's pilot
        ship.RefreshPilot(this);

        //  refresh ship weapons
        ship.RefreshWeapons();
    }

    public void GetShipComponents()
    {
        //  get ship components and transform
        shipTrans = transform.parent;
        ship = shipTrans.gameObject.GetComponent<Ship>();
        shipRigBody = shipTrans.gameObject.GetComponent<Rigidbody2D>();

        //  if autopilot, find other pilot in ship
        if (autopilot)
        {
            Pilot[] pilots = transform.parent.GetComponentsInChildren<Pilot>();
            for (int i = 0; i < pilots.Length; i++)
            {
                if (pilots[i] != this)
                {
                    autopilotPilot = pilots[i];
                }
            }
        }
    }

    //  turn thrust animation on/off
    public void Thrust(bool on)
    {
        if (thrustOn != on && ship.thrustObject != null)
        {
            thrustOn = on;
            ship.thrustObject.SetActive(on);
        }
    }

    //  moving towards
    public bool MovingTowards(Vector2 point, float threshold = 30)
    {
        Quaternion direction = SystmAPI.Geometry.GetDirection(shipRigBody.velocity, shipRigBody.velocity * 2);
        Quaternion toTarget = SystmAPI.Geometry.GetDirection(shipTrans.position, point);

        if (Quaternion.Angle(direction, toTarget) < threshold)
        {
            return true;
        }
        else { return false; }
    }

    //  moving away
    public bool MovingAway(Vector2 point)
    {
        float current = Vector2.Distance(point, shipTrans.position);
        float previous = Vector2.Distance(point, previousPosition);

        if (current > previous)
        {
            return true;
        }
        else { return false; }
    }

    //  moving
    public bool Moving()
    {
        if (previousPosition != transform.position)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    //  mouse position
    public virtual Vector2 MousePosition()
    {
        return Vector2.zero;
    }

    #endregion

    //  //
    #region AI
    //  //

    //  follow
    public virtual void FollowLeader(bool now = false) { }

    //  attack
    public virtual void Attack(SystmObject target, bool now = false) { }

    //  land
    public virtual void Land(Landable landable) { }
    public virtual void LandComplete(Landable landable) { }

    //  custom updates
    public virtual void SlowUpdate() { }
    public virtual void SlowerUpdate() { }

    //DEBUG

    public virtual void Stop(bool stop = true) { }
    public virtual void Align(SystmObject target) { }

    public virtual void IssueCommand(ShipBehaviour command, bool now) { }

    public virtual void DefaultBehaviour() { }

    #endregion

    //  //
    #region Targeting
    //  //

    //  targeted by another object
    public virtual void TargetedBy(SystmObject sysObj, bool targeted) { }

    //  lock target
    public virtual void NewTarget(SystmObject target)
    {
        ship.pilot.DropTarget();

        NewEnemy(target.pilot);
        ship.target = target;
        target.SystmObjectTargetedBy(ship, true);
    }

    //  drop locked target
    public virtual void DropTarget()//int group = 0)
    {
        if (ship.target != null)
        {
            ship.target.SystmObjectTargetedBy(ship, false);//, group);
            ship.target = null;
        }
    }

    //  targeted object has changed
    public virtual void UpdateTarget(SystmObject target, bool dead)
    {
        if (dead)
        {
            DropTarget();
        }
    }

    //  add pilot to list of enemies
    public void NewEnemy(Pilot pilot)
    {
        if (!enemies.Contains(pilot))
        {
            enemies.Add(pilot);
        }
    }

    #endregion

    //  //
    #region Movement
    //  //
    
    public Vector3 previousPosition;

    //  left
    public void Left()
    {
        shipTrans.Rotate(Vector3.forward * ship.turn);
    }

    //  right
    public void Right()
    {
        shipTrans.Rotate(-Vector3.forward * ship.turn);
    }

    //  stop
    public bool Stop()
    {
        if (shipRigBody.velocity.magnitude != 0)
        {
            float clamp = shipRigBody.velocity.magnitude - SystmAPI.Physics.Acceleration(this) * Time.deltaTime;
            shipRigBody.velocity = Vector2.ClampMagnitude(shipRigBody.velocity, clamp);
            return false;
        }
        else
        {
            return true;
        }
    }

    //  break
    public bool Decelerate()
    {
        if (shipRigBody.velocity.magnitude != 0)
        {
            shipRigBody.drag += ship.thrust / 5;
            return false;
        }
        else
        {
            return true;
        }
    }
    public bool StopDecelerate() { shipRigBody.drag = 0; return false; }

    //  accel
    public void Accelerate(float maxSpeed = 0)
    {
        if (maxSpeed == 0)
        {
            maxSpeed = ship.speed;
        }
        Thrust(true);
        shipRigBody.AddForce(shipTrans.up * ship.thrust);
        shipRigBody.velocity = Vector2.ClampMagnitude(shipRigBody.velocity, maxSpeed);
    }

    //  strafe
    public void StrafeRight()
    {
        shipRigBody.AddForce(shipTrans.right * (ship.thrust / 2));
        shipRigBody.velocity = Vector2.ClampMagnitude(shipRigBody.velocity, ship.speed);
    }
    public void StrafeLeft()
    {
        shipRigBody.AddForce(-shipTrans.right * (ship.thrust / 2));
        shipRigBody.velocity = Vector2.ClampMagnitude(shipRigBody.velocity, ship.speed);
    }
    public void StrafeForward()
    {
        shipRigBody.AddForce(shipTrans.up * (ship.thrust / 2));
        shipRigBody.velocity = Vector2.ClampMagnitude(shipRigBody.velocity, ship.speed);
    }
    public void StrafeBackward()
    {
        shipRigBody.AddForce(-shipTrans.up * (ship.thrust / 2));
        shipRigBody.velocity = Vector2.ClampMagnitude(shipRigBody.velocity, ship.speed);
    }

    //  face opposite to direction of travel
    public bool FaceBackwards() { return FaceDirection(SystmAPI.Geometry.GetDirection(shipRigBody.velocity, shipRigBody.velocity * 2)); }

    //  face direction of rotation
    public bool FaceDirection(Quaternion dir)
    {
        //Rotation relative to desired direction
        if (!SystmAPI.Generic.InRange(Quaternion.Angle(shipTrans.rotation, dir), 0, ship.turn))
        {
            if (SystmAPI.Geometry.TurnRight(shipTrans.rotation, dir))
                Right();
            else
                Left();
            return false;
        }
        else
        {
            if (shipTrans.rotation != dir)
                shipTrans.rotation = dir;
            return true;
        }
    }
    
    #endregion

    //  //
    #region Attack
            //  //

    //  start/stop firing
    public void FirePrimaryDown()
    {
        firingPrimary = true;
        ship.FirePrimaryDown();
    }
    public void FirePrimaryUp()
    {
        firingPrimary = false;
        ship.FirePrimaryUp();
    }
    public void FireSecondaryDown()
    {
        firingSecondary = true;
        ship.FireSecondaryDown();
    }
    public void FireSecondaryUp()
    {
        firingSecondary = false;
        ship.FireSecondaryUp();
    }
    #endregion

    //  //
    #region Groups
    //  //

    public virtual void UpdateMember(SystmObject member, bool dead) { }

    #endregion
}