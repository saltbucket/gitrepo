﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SerializeField]
public class Fleet
{
    public Pilot commander;

    //  management
    public List<Pilot>[] groups;
    public Pilot[] leaders;
    public bool[] groupActive;

    //  combat
    public Dictionary<Fleet, List<Combat>> enemies;

    public Fleet(Pilot cmmander, int gCount)
    {
        commander = cmmander;

        //  management
        groups = new List<Pilot>[gCount];
        leaders = new Pilot[gCount];
        groupActive = new bool[gCount];

        for (int i = 0; i < gCount; i++)
        {
            groups[i] = new List<Pilot>();
            groupActive[i] = false;
        }

        //  combat
        enemies = new Dictionary<Fleet, List<Combat>>();

    }
    
    //  //
    #region Generic
    //  //

    public List<SystmObject> GroupShips(int group)
    {
        List<SystmObject> ships = new List<SystmObject>();
        foreach (Pilot pilot in groups[group])
        {
            ships.Add(pilot.ship);
        }
        return ships;
    }
    public List<Pilot> GroupPilots(int group)
    {
        List<Pilot> pilots = new List<Pilot>();
        foreach (Pilot pilot in groups[group])
        {
            pilots.Add(pilot);
        }
        return pilots;
    }

    //  return true if pilot is in fleet
    public bool HasMember(Pilot pilot)
    {
        int instanceID = pilot.GetInstanceID();
        for (int i = 0; i < groups.Length; i++)
        {
            foreach (Pilot member in groups[i])
            {
                if (member.GetInstanceID() == instanceID)
                {
                    return true;
                }
            }
        }
        return false;
    }

    
    // ? ? ?
    public Dictionary<Fleet, List<Pilot>> GetFleets(List<Pilot> pilots)
    {
        Fleet orphans = null;
        Dictionary<Fleet, List<Pilot>> fleets = new Dictionary<Fleet, List<Pilot>>();

        foreach (Pilot pilot in pilots)
        {
            if (pilot.FleetMember())
            {
                if (fleets.ContainsKey(pilot.fleet))
                {
                    fleets[pilot.fleet].Add(pilot);
                }
                else
                {
                    fleets[pilot.fleet] = new List<Pilot> { pilot };
                }
            }
            else
            {
                if (orphans == null)
                {
                    fleets[new Fleet(pilot, 1)] = new List<Pilot>();
                }
                else
                {
                    orphans.AddMember(pilot);
                }
                fleets[orphans].Add(pilot);
            }
        }
        return fleets;
    }

    #endregion

    //  //
    #region Management
    //  //

    public List<Pilot> AllMembers()
    {
        List<Pilot> allMembers = new List<Pilot>();

        for (int i = 0; i < groups.Length; i++)
        {
            allMembers.AddRange(groups[i]);
        }
        return allMembers;
    }

    public void AddMember(Pilot member, int group = 0)
    {
        if (leaders[group] == null)
        {
            leaders[group] = member;
            if (groups[group].Count == 0)
            {
                groupActive[group] = true;
            }
        }
        groups[group].Add(member);
        member.fleet = this;
        member.groupNumber = group;
    }

    public void RemoveMember(Pilot member)
    {
        int group = member.groupNumber;
        groups[group].Remove(member);

        if (groups[group].Count > 0)
        {
            if (leaders[member.groupNumber] == member)
            {
                leaders[group] = FindNewLeader(group);
            }
        }
        else
        {
            groupActive[group] = false;
        }
        
    }
    private Pilot FindNewLeader(int group)
    {
        return groups[group][0];
    }

    #endregion

    //  //
    #region Combat
    //  //

    //  DON'T ORGANISE COMBATS BY FLEET, A COMBAT SHOULD NOT NEED TO INVOLVE ONLY A SINGLE ENEMY FLEET.

    

    public void Attack(List<Pilot> members, List<Pilot> defenders)
    {
        foreach (Pilot member in members)
        {
            foreach (Pilot defender in defenders)
            {
                Debug.Log(member.name + " attacking " + defender.ship);
                member.Attack(defender.ship, true);
            }
        }
    }

    #endregion

    //  //
    #region Commands
    //  //

    public void FollowLeader(List<Pilot> members)
    {
        foreach (Pilot member in members)
        {
            member.FollowLeader();
        }
    }

    /*public void Follow(List<Pilot> members)
    {
        foreach (Pilot member in members)
        {
            member.Follow(commander.ship);
        }
    }*/

    public void Stop(List<Pilot> members)
    {
        foreach (Pilot member in members)
        {
            member.Stop();
        }
    }

    #endregion

}
