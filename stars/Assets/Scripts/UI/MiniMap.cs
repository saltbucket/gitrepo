﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class MiniMap : MonoBehaviour
{

    //public float shipScale = 0.03f;

    [Header(":: Minimap")]
    public GameManager game;

    public GameObject mmShip;
    public GameObject mmPlanet;
    protected Dictionary<string, GameObject> mmImages;
    public GameObject uiParent;
    private GameObject shipFollower;

    protected List<MinimapObject> miniMapObjects;

    private void Awake()
    {
        //build dictionary to match prefabs with tag strings
        mmImages = new Dictionary<string, GameObject>();
        mmImages.Add("ship", mmShip);
        mmImages.Add("planet", mmPlanet);
        mmImages.Add("DEBUGOBJ", mmShip); //DEBUG

        miniMapObjects = new List<MinimapObject>();

        foreach (SystmObject systmObject in game.systm.systmObjects)
        {
            systmObject.mmObject = NewMinimapObj(systmObject.gameObject);
        }

        shipFollower = game.player.shipFollower;
    }
    
    //update all minimap image positions
    public void RefreshPositions()
    {
        foreach (MinimapObject mmObj in miniMapObjects)
            mmObj.UpdatePosition(shipFollower);
    }

    //  object status changed by systm
    public void RefreshMinimapObj(SystmObject systmObject)
    {
        if (systmObject.pilot.enemies.Contains(game.systm.player))
        {
            systmObject.mmObject.ChangeColor(new Color(1, 0, 0));
        }
    }

    //Spawn minimap images (recycle eventually)
    public MinimapObject NewMinimapObj(GameObject gObj)//, GameObject imagePrefab)
    {
        MinimapObject obj = new MinimapObject();

        obj.worldObj = gObj;

        obj.mmImage = Instantiate(mmImages[gObj.tag]);
        obj.mmImage.transform.SetParent(uiParent.transform, false);
        obj.mmImage.name = "MM " + gObj.name;

        //  color object
        obj.ChangeColor(new Color(0, 1, 0));

        obj.mmImage.SetActive(true);

        miniMapObjects.Add(obj);

        return obj;
    }
    
    //destroy minimap image and remove from list
    public void DestroyMinimapObj(MinimapObject mmObj)
    {
        miniMapObjects.Remove(mmObj);
        Destroy(mmObj.mmImage);
    }

    public class MinimapObject
    {
        public GameObject mmImage;
        public GameObject worldObj;
        //public Color color;

        public float size;
        public int hostility; //3-red- hostile to you // 2-yellow- hostile to other // 1-green- not hostile
        public bool targeted;

        public void ChangeColor(Color color)
        {
            mmImage.GetComponent<Image>().color = color;
        }
        
    //Update minimap image position
        public void UpdatePosition(GameObject player)
        {
            Vector3 relativeToPlayer = player.transform.InverseTransformPoint(worldObj.transform.position);
            mmImage.transform.localPosition = relativeToPlayer * 10;
        }
        
            
        
    }


}
