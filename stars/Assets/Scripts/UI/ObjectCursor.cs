﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectCursor
{
    private RectTransform rectTransform;
    public Bounds bounds;
    private Camera camera;
    public SystmObject obj;
    public GameObject cursor;

    //  constructor
    public ObjectCursor(Camera cmra, SystmObject ob, GameObject crsrObj)
    {
        camera = cmra;
        obj = ob;
        cursor = crsrObj;
        bounds = obj.bounds;
        rectTransform = cursor.GetComponent<RectTransform>();
    }
    
    //  size of bounds in screen space
    private Vector2 GetScreenSize(Bounds bounds)
    {
        Vector2 topright = camera.WorldToScreenPoint(new Vector2(bounds.center.x + bounds.extents.x, bounds.center.y + bounds.extents.y));
        Vector2 bottomleft = camera.WorldToScreenPoint(new Vector2(bounds.center.x - bounds.extents.x, bounds.center.y - bounds.extents.y));
        return topright - bottomleft;
    }

    //  match cursor rect to ship image size in screen space
    public void UpdateCursorSize()
    {
        Vector2 size = GetScreenSize(bounds);
        if (size.x < 50)
        {
            rectTransform.sizeDelta = new Vector2(50, 50);
        }
        else
        {
            rectTransform.sizeDelta = size;
        }
    }

    //  set cursor position to ship position in screen space
    public void UpdateCursorPosition()
    {
        cursor.transform.localPosition = (Vector2)camera.WorldToScreenPoint(obj.transform.position) - SystmAPI.Generic.CanvasOffset();
        cursor.transform.rotation = obj.transform.rotation;
    }

    //  change cursor sprite
    public void SetCursorSprite(Sprite sprite)
    {
        foreach (Transform child in cursor.transform)
        {
            child.transform.GetChild(0).GetComponent<Image>().sprite = sprite;
            child.localRotation = new Quaternion();
        }
    }

    //  change cursor color
    public void SetCursorColor(Color color)
    {
        foreach (Transform child in cursor.transform)
        {
            child.transform.GetChild(0).GetComponent<Image>().color = color;
        }
    }
    public void SetCursorAlpha(int alpha32)
    {
        foreach (Transform child in cursor.transform)
        {
            SystmAPI.Generic.SetImageAlpha(child.transform.GetChild(0).GetComponent<Image>(), alpha32);
        }
    }
}
