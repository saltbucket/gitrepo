﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [Header("UI Manager")]
    public PlayerShipController player;
    public Camera spaceCamera;
    public GameManager game;
    public UIStyles.UIStyle uiStyle;


    //  UI scripts
    public TargetPanelManager targetPanels;
    public FleetPanel fleetPanel;
    public CommandMode commandMode;
    public TargetBrowser targetBrowser;
    public ObjectCursorManager objectCursors;
    
    //  Other
    [HideInInspector]
    public int[] highlightAlphas;
    public bool zoomLocked;
    public IEnumerator zoomCoroutine;

    [Header("Fleet Panel")]
    public GameObject fleetPanelParent;

    //  current objects
    [SerializeField]
    public List<SystmObject> currentBrowsing;
    [SerializeField]
    public List<SystmObject> currentChosenList;
    

    [Header("Target Browser")]
    public Text targetBrowser_title;
    public Text targetBrowser_tooltip;
    public Text targetBrowser_primary;
    public Text targetBrowser_secondary;
    public GameObject filterPanel;
    public GameObject otherChosenPanel;

    [Header("Target Panels")]
    public GameObject smallTargetPanelPrefab;
    private List<TargetPanel> storedSmallTargetPanels = new List<TargetPanel>();

    [Header("Commands")]
    public CommandMenu commandMenu;
    public GameObject commandMenuParent;
    public GameObject commandButtonPrefab;

    [Header("Highlights")]
    public GameObject highlightPrefab;
    private List<GameObject> spareHighlights = new List<GameObject>();
    private Dictionary<GameObject, GameObject> assignedHighlights = new Dictionary<GameObject, GameObject>();

    [Header("Cursors")]
    public GameObject objectCursorPrefab;

    [Header("Tab Menu")]
    public GameObject tabParent;
    public TabMenuManager tabMenu;
    public FleetTab fleetTab;

    //  browsing and choosing
    private List<SystmObject> objectsHighlighted = new List<SystmObject>();
    [SerializeField]
    public List<SystmObject> membersChosen = new List<SystmObject>();
    [SerializeField]
    public List<SystmObject> otherChosen = new List<SystmObject>();
    
    //  //
    #region Generic 
    //  //

    //  instantiate UI panel scripts
    private void Awake()
    {
        targetPanels = new TargetPanelManager(this);
        fleetPanel = new FleetPanel(this, fleetPanelParent, targetPanels.memberPanels, membersChosen);
        commandMode = new CommandMode(this);
        targetBrowser = new TargetBrowser(this, targetBrowser_title, targetBrowser_tooltip, targetBrowser_primary, targetBrowser_secondary, filterPanel);
        objectCursors = new ObjectCursorManager(this, objectCursorPrefab);
        commandMenu = new CommandMenu(this, commandMenuParent, commandButtonPrefab);
        zoomLocked = false;

        tabMenu = new TabMenuManager(tabParent, game);
        fleetTab = tabMenu.parent.GetComponentInChildren<FleetTab>(includeInactive: true);

        highlightAlphas = new int[] { 75, 150, 255 };
        ResetBrowsing();
        
    }

    //  gather lists of UI object and set to current style
    public void ChangeStyle(UIStyles.UIStyle uStyle)
    {
        uiStyle = uStyle;
        Image[] uiImages = GetComponentsInChildren<Image>(includeInactive: true);
        Text[] uiTexts = GetComponentsInChildren<Text>(includeInactive: true);
        
        List<Image> panels = new List<Image>();
        List<Image> subPanels = new List<Image>();
        List<Image> tintedPanels = new List<Image>();
        

        for (int i = 0; i < uiImages.Length; i++)
        {
            if (uiImages[i].tag == "uiPanel")
            {
                panels.Add(uiImages[i]);
            }
            else if (uiImages[i].tag == "uiSubPanel")
            {
                subPanels.Add(uiImages[i]);
            }
            else if (uiImages[i].tag == "uiTintedPanel")
            {
                tintedPanels.Add(uiImages[i]);
            }
        }

        uiStyle.SetPanelStyles(panels);
        uiStyle.SetSubPanelStyles(subPanels);
        uiStyle.SetTintedPanelStyles(tintedPanels);
        uiStyle.SetTextStyles(uiTexts);
    }

    //  UI cursors updated on late update to avoid graphics glitch
    private void LateUpdate()
    {
        objectCursors.CursorSizeUpdate();
        objectCursors.CursorPositionUpdate();
    }

    //  for use with UI scripts that don't inheret Monobehaviour
    public GameObject NewPanel(GameObject prefab, Transform parent)
    {
        return Instantiate(prefab, parent) as GameObject;
    }
    public void RemovePanel(GameObject panel)
    {
        Destroy(panel);
    }

    public bool NoMenusOpen()
    {
        if (fleetPanel.active || commandMenu.active)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public GameObject SpawnPrefab(GameObject prefab, Transform parent)
    {
        return Instantiate(prefab, parent);
    }
    public void DestroyObject(GameObject obj)
    {
        Destroy(obj);
    }

    #endregion

    //  //
    #region Choose 
    //  //

    public void ResetBrowsing()
    {
        currentBrowsing = game.systm.ships;
        currentChosenList = otherChosen;

        targetBrowser.ClearBrowser();

        if (commandMode.active)
        {
            commandMode.Exit();
        }
    }

    public void ClearAllChosen()
    {
        ClearChosen(membersChosen);
        ClearChosen(otherChosen);
    }

    //  return correct color for ship's current status
    public Color GetStatusColor(SystmObject systmObject)
    {
        if (player.ship.EnemyOf(systmObject))
        {
            return player.settings.colorRed;
        }
        else if (systmObject.HasCommander(player.ship))
        {
            return player.settings.colorGreen;
        }
        else
        {
            return player.settings.colorBlue;
        }
    }
    
    //  check if systmObject is in chosen lists
    public bool IsChosen(SystmObject systmObject)
    {
        if (membersChosen.Contains(systmObject) || otherChosen.Contains(systmObject))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public List<SystmObject> GetChosenList(SystmObject systmObject)
    {
        if (membersChosen.Contains(systmObject))
        {
            return membersChosen;
        }
        else if (otherChosen.Contains(systmObject))
        {
            return otherChosen;
        }
        return null;
    }

    //  check if objects are chosen
    public bool MembersChosen(List<SystmObject> chosenList = null)
    {

        if (membersChosen.Count > 0)
        {
            return true;
        }
        else
        {
            return false;

        }
    }
    public bool OthersChosen(List<SystmObject> chosenList = null)
    {

        if (otherChosen.Count > 0)
        {
            return true;
        }
        else
        {
            return false;

        }
    }

    //  highlight
    public void HighlightObject(SystmObject systmObject)
    {
        //  check - add
        if (objectsHighlighted.Contains(systmObject))
        {
            return;
        }
        else
        {
            objectsHighlighted.Add(systmObject);
        }

        //  panels
        targetPanels.HighlightPanels(systmObject, true);

        //  cursor
        if (commandMode.active)
        {
            objectCursors.HighlightCursor(systmObject, 1);
        }
        else
        {
            objectCursors.AddCursor(systmObject);
        }
        
    }

    //  unhighlight
    public void UnhighlightObject(SystmObject systmObject)
    {
        //  check - remove
        if (!objectsHighlighted.Contains(systmObject))
        {
            return;
        }
        else
        {
            objectsHighlighted.Remove(systmObject);
        }

        //  panels
        targetPanels.HighlightPanels(systmObject, false);

        //  cursor
        if (commandMode.active)
        {
            objectCursors.HighlightCursor(systmObject, 0);
        }
        else
        {
            objectCursors.RemoveCursor(systmObject);
        }
    }

    public void UnhighlightAllObjects()
    {
        foreach (SystmObject systmObject in SystmAPI.Generic.CopyObjectList(objectsHighlighted))
        {
            UnhighlightObject(systmObject);
        }
    }

    //  choose
    public void ChooseObject(SystmObject systmObject, List<SystmObject> chosenList)
    {
        //  add
        chosenList.Add(systmObject);
        
        //  panels
        if (chosenList == otherChosen)
        {
            targetPanels.AddPanel(systmObject, targetPanels.otherChosenPanels, otherChosenPanel);
        }
        else
        {
            targetPanels.ChoosePanels(systmObject, true);
        }

        //  cursor
        if (!objectCursors.HasCursor(systmObject))
        {
            objectCursors.AddCursor(systmObject);
        }
        objectCursors.HighlightCursor(systmObject, 2);

    }

    //  unchoose
    public void UnChooseObject(SystmObject systmObject, List<SystmObject> chosenList)
    {
        //  add
        chosenList.Remove(systmObject);

        //  panels
        if (chosenList == otherChosen)
        {
            targetPanels.RemovePanel(systmObject, targetPanels.otherChosenPanels);
        }
        else
        {
            targetPanels.ChoosePanels(systmObject, false);
        }

        //  cursor
        if (commandMode.active)
        {
            objectCursors.HighlightCursor(systmObject, 0);
        }
        else
        {
            objectCursors.RemoveCursor(systmObject);
        }
    }

    //  choose highlighted
    public void ChooseHighlighted()
    {
        if (objectsHighlighted.Count > 0)
        {
            ChooseObjectList(objectsHighlighted);
        }
        objectsHighlighted.Clear();
    }

    //  choose list
    public void ChooseObjectList(List<SystmObject> chosenObjects, List<SystmObject> chosenList = null)
    {
        if(chosenList == null)
        {
            chosenList = currentChosenList;
        }

        foreach (SystmObject systmObject in chosenObjects)
        {
            ChooseObject(systmObject, chosenList);
        }
    }

    //  clear
    public void ClearChosen(List<SystmObject> chosenList = null)
    {
        if (chosenList == null)
        {
            chosenList = currentChosenList;
        }
        foreach (SystmObject member in SystmAPI.Generic.CopyObjectList(chosenList))
        {
            UnChooseObject(member, chosenList);
        }
    }

    public void ClearObject(SystmObject systmObject)
    {
        if (IsChosen(systmObject))
        {
            UnChooseObject(systmObject, GetChosenList(systmObject));
        }
        if (objectsHighlighted.Contains(systmObject))
        {
            UnhighlightObject(systmObject);
        }
    }

    #endregion

    //  //
    #region Target panels 
    //  //

    public void UpdateTargetStats(SystmObject systmObject, bool dead)
    {
        if (targetBrowser.browserPanel.target == systmObject)
        {
            if (dead)
            {
                targetBrowser.ClearBrowser();
            }
            else
            {
                targetBrowser.browserPanel.UpdateTargetStats();
            }
        }

        targetPanels.UpdatePanels(systmObject, dead);

        if (dead)
        {
            UnhighlightObject(systmObject);
            ClearObject(systmObject);
        }
    }

    #endregion

    //  //
    #region Zoom 
    //  //

    public void StartZoom(float amount, bool zOut, bool locked)
    {
        if (locked)
        {
            zoomLocked = true;
        }

        if (zOut)
        {
            zoomCoroutine = ZoomOut(player.ship.shipClass + amount, locked);
        }
        else
        {
            zoomCoroutine = ZoomIn(player.ship.shipClass + amount, locked);
        }
        StartCoroutine(zoomCoroutine);
    }

    public void StopZoom()
    {
        if (zoomCoroutine != null)
        {
            StopCoroutine(zoomCoroutine);
        }
    }

    IEnumerator ZoomOut(float amount, bool locked)
    {
        while (spaceCamera.orthographicSize < amount)
        {
            spaceCamera.orthographicSize += (amount - (spaceCamera.orthographicSize - 1)) / 30;
            
            yield return null;
        }
        if (locked)
        {
            zoomLocked = false;
        }
    }

    IEnumerator ZoomIn(float amount, bool locked)
    {
        while (spaceCamera.orthographicSize > amount)
        {
            spaceCamera.orthographicSize -= (amount + (spaceCamera.orthographicSize - 1)) / 30;
            yield return null;
        }
        if (locked)
        {
            zoomLocked = false;
        }
    }

    #endregion

    //  //
    #region Highlights 
    //  //

    //  get/create for recycling
    private GameObject GetHighLight()
    {
        if (spareHighlights.Count == 0)
        {
            return Instantiate(highlightPrefab, gameObject.transform);
        }
        else
        {
            GameObject highlight = spareHighlights[0];
            spareHighlights.Remove(highlight);
            return highlight;
        }
    }

    //  store/delete highlight
    public void UnHighlightPanel(GameObject panel)
    {
        if (!assignedHighlights.ContainsKey(panel))
        {
            return;
        }
        if (spareHighlights.Count > 4)
        {
            Destroy(assignedHighlights[panel]);
        }
        else
        {
            assignedHighlights[panel].SetActive(false);
            spareHighlights.Add(assignedHighlights[panel]);
        }
        assignedHighlights.Remove(panel);
    }
    
    //  get/create highlight
    public void HighlightPanel(GameObject panel, Color color)
    {
        if (assignedHighlights.ContainsKey(panel))
        {
            return;
        }

        RectTransform panelRect = panel.GetComponent<RectTransform>();

        GameObject highlight = GetHighLight();
        assignedHighlights[panel] = highlight;
        RectTransform highlightRect = highlight.GetComponent<RectTransform>();
        
        highlight.transform.SetParent(panel.transform);
        highlightRect.pivot = panelRect.pivot;
        highlightRect.position = panelRect.position;
        highlightRect.sizeDelta = new Vector2();
        highlight.SetActive(true);

        SetHighlightColor(highlight, color);
    }

    public void SetHighlightColor(GameObject parentPanel, Color color)
    {
        Image[] images = parentPanel.GetComponentsInChildren<Image>();
        foreach (Image image in images)
        {
            image.color = color;
        }
    }
    public void SetHighLightAlpha(GameObject parentPanel, int level)
    {
        Image[] images = assignedHighlights[parentPanel].GetComponentsInChildren<Image>();
        foreach (Image image in images)
        {
            SystmAPI.Generic.SetImageAlpha(image, player.settings.ui_HighlightAlphas[level]);
        }
    }

    #endregion

    
    
    
}
