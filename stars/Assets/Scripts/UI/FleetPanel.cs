﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class FleetPanel : IPlayerInput
{
    //  UI panels
    private UIManager uiManager;
    private GameObject parentPanel;

    private List<GameObject> groupPanels = new List<GameObject>();
    private Dictionary<SystmObject, TargetPanel> panelDict;
    private List<SystmObject> chosenList;

    public bool active = false;
    private int currentGroup;

    //DEBUG
    public void CreateDebugGroup()//DEBUG
    {
        uiManager.fleetPanel = this;

        GameObject[] debugGROUP = GameObject.FindGameObjectsWithTag("DEBUGOBJ"); //DEBUG
        foreach (GameObject gobj in debugGROUP)
        {
            uiManager.player.NewFleetMember(gobj.GetComponent<SystmObject>(), 0);//DEBUG
        }
    }//DEBUG

    public FleetPanel(UIManager uManager, GameObject pPanel, Dictionary<SystmObject, TargetPanel> pDict, List<SystmObject> cList)
    {
        uiManager = uManager;
        parentPanel = pPanel;
        panelDict = pDict;
        chosenList = cList;
        
        groupPanels = SystmAPI.Generic.GetChildren(parentPanel.transform);
    }
    
    //  //
    #region Generic
    //  //

    //  enter
    public void Enter()
    {
        if (active)
        {
            return;
        }

        active = true;
        uiManager.player.inputs.Add(this);

        uiManager.currentChosenList = chosenList;
        FindGroup();
    }

    //  exit
    private void Exit()
    {

        active = false;
        uiManager.player.inputs.Remove(this);

        uiManager.ResetBrowsing();
    }

    //  count active group panels
    private int ActivePanelsCount()
    {
        int count = 0;
        foreach (GameObject panel in groupPanels)
        {
            if (panel.activeInHierarchy)
            {
                count += 1;
            }
        }
        return count;
    }

    //  member list
    private List<SystmObject> GroupMembers(int group)
    {
        return uiManager.player.fleet.GroupShips(group);
    }

    //  add
    public void AddPanel(SystmObject member, int group)
    {
        ManagePanels(group, true);
        uiManager.targetPanels.AddPanel(member, panelDict, groupPanels[group]);
    }

    //  remove
    public void RemovePanel(SystmObject member)
    {
        uiManager.targetPanels.RemovePanel(member, panelDict);
        ManagePanels(member.Group(), false);
    }

    //  activate
    private void ManagePanels(int group, bool activate)
    {
        if (panelDict.Count == 0)
        {
            parentPanel.SetActive(activate);
        }
        if (groupPanels[group].transform.childCount == 0)
        {
            groupPanels[group].gameObject.SetActive(activate);
        }
    }

    //  update panel config (change parent)
    public void UpdateMemberConfig(SystmObject member)
    {
        uiManager.targetPanels.GetMemberPanel(member).transform.SetParent(groupPanels[member.Group()].transform);
    }

    //  check if all are chosen
    private bool AllChosen(int group)
    {
        List<SystmObject> members = GroupMembers(group);
        if (members.Count > 0)
        {
            foreach (SystmObject member in members)
            {
                if (!chosenList.Contains(member))
                {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    #endregion

    //  //
    #region UI Actions
    //  //

    //  find available group
    private void FindGroup()
    {
        for (int i = 0; i < ActivePanelsCount(); i++)
        {
            if (!AllChosen(i))
            {
                SelectGroup(i);
                return;
            }
        }
        Exit();
    }

    //  choose member within group
    private void SelectGroup (int group)
    {
        //  don't select empty group
        if (GroupMembers(group).Count == 0)
        {
            return;
        }
        currentGroup = group;
        
        uiManager.currentBrowsing = (GroupMembers(group));

        uiManager.targetBrowser.BrowseCurrent(true);
    }
        
    #endregion

    //  //
    #region UI Update
    //  //

    //  run in update while this is active
    public void InputUpdate()
    {
        if (Input.GetKeyDown(uiManager.player.settings.fleetPanel))
        {
            Exit();
        }
        if (Input.GetKeyDown(uiManager.player.settings.action))
        {
            uiManager.ChooseHighlighted();
            if (AllChosen(currentGroup))
            {
                FindGroup();
            }
            else
            {
                uiManager.targetBrowser.BrowseCurrent(true);
            }
        }
        //  esc
        if (Input.GetKeyDown(uiManager.player.settings.escape))
        {
            if (chosenList.Count > 0)
            {
                uiManager.ClearChosen();
            }
            else
            {
                Exit();
            }
        }
        else
        {
            //  group numbers
            if (Input.GetKeyDown(uiManager.player.settings.ship_a1))
            {
                GroupInput(0);
            }
            else if (Input.GetKeyDown(uiManager.player.settings.ship_a2))
            {
                GroupInput(1);
            }
        }        
    }

    //  numeric group keys
    private void GroupInput(int group)
    {
        if (Input.GetKey(uiManager.player.settings.alternate))
        {
            uiManager.ChooseObjectList(GroupMembers(group));
        }
        else
        {
            SelectGroup(group);
        }
    }



    #endregion
}
