﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CommandMode : IPlayerInput
{
    public bool active = false;
    private UIManager uiManager;

    public CommandMode(UIManager uManager)
    {
        uiManager = uManager;
    }

    //  enter
    public void EnterCommandMode()
    {
        ToggleCommandMode(true);
        uiManager.player.ship.StopWeapons();
        foreach (SystmObject systmObject in uiManager.game.systm.systmObjects)
        {
            if (uiManager.IsChosen(systmObject))
            {
                continue;
            }
            uiManager.objectCursors.AddCursor(systmObject);
            uiManager.objectCursors.HighlightCursor(systmObject, 0);
        }

        uiManager.StartZoom(10, true, false);
    }

    //  exit
    public void Exit()
    {
        ToggleCommandMode(false);

        foreach (SystmObject systmObject in uiManager.game.systm.systmObjects)
        {
            if (uiManager.IsChosen(systmObject))
            {
                continue;
            }
            uiManager.objectCursors.RemoveCursor(systmObject);
        }

        uiManager.StopZoom();

        float maxZoom = uiManager.player.MaxZoom(false);
        if (uiManager.spaceCamera.orthographicSize > maxZoom)
        {
            uiManager.StartZoom((uiManager.spaceCamera.orthographicSize - maxZoom), false, true);

        }
    }

    private void ToggleCommandMode(bool on)
    {
        if (active == on)
        {
            return;
        }
        active = on;
        uiManager.game.Pause(on);
        if (on)
        {
            uiManager.player.inputs.Add(this);
        }
        else
        {
            uiManager.player.inputs.Remove(this);

        }
    }

    //  scroll faster when zoomed out
    private Vector3 AdjustScrollSpeed(Vector2 direction)
    {
        return (direction * uiManager.spaceCamera.orthographicSize) * Time.fixedDeltaTime;
    }

    //  player input interface
    public void InputUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            Exit();
        }

        if (Input.GetKey(uiManager.player.settings.ship_Accel))
        {
            uiManager.spaceCamera.transform.position += AdjustScrollSpeed(Vector3.up);
        }
        else if (Input.GetKey(uiManager.player.settings.ship_Break))
        {
            uiManager.spaceCamera.transform.position -= AdjustScrollSpeed(Vector3.up);
        }

        if (Input.GetKey(uiManager.player.settings.ship_Right))
        {
            uiManager.spaceCamera.transform.position += AdjustScrollSpeed(Vector3.right);
        }
        else if (Input.GetKey(uiManager.player.settings.ship_Left))
        {
            uiManager.spaceCamera.transform.position -= AdjustScrollSpeed(Vector3.right);
        }

    }

    
}
