﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectCursorManager
{
    private UIManager uiManager;
    private GameObject cursorPrefab;

    private Dictionary<SystmObject, ObjectCursor> activeCursors = new Dictionary<SystmObject, ObjectCursor>();
    private List<ObjectCursor> storedCursors = new List<ObjectCursor>();

    public ObjectCursorManager(UIManager uManager, GameObject cPrefab)
    {
        uiManager = uManager;
        cursorPrefab = cPrefab;
    }

    //  object has cursor
    public bool HasCursor(SystmObject systmObject)
    {
        if (activeCursors.ContainsKey(systmObject))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    //  highlight
    public void HighlightCursor(SystmObject systmObject, int level)
    {
        if (activeCursors.ContainsKey(systmObject))
        {
            activeCursors[systmObject].SetCursorAlpha(uiManager.highlightAlphas[level]);
        }
    }

    //   get cursor from recycling
    public void AddCursor(SystmObject systmObject)
    {
        if (activeCursors.ContainsKey(systmObject))
        {
            return;
        }

        ObjectCursor cursor;
        if (storedCursors.Count == 0)
        {
            GameObject cursorObject = uiManager.NewPanel(cursorPrefab, uiManager.transform);
            cursor = new ObjectCursor(uiManager.spaceCamera, systmObject, cursorObject);
        }
        else
        {
            cursor = storedCursors[0];
            storedCursors.Remove(cursor);

            cursor.obj = systmObject;
            cursor.bounds = systmObject.bounds;
            cursor.cursor.SetActive(true);
        }

        cursor.SetCursorSprite(systmObject.uiImage);
        cursor.SetCursorColor(uiManager.GetStatusColor(systmObject));
        cursor.SetCursorAlpha(uiManager.highlightAlphas[0]);

        activeCursors[systmObject] = cursor;
    }

    //  store cursor in recycling
    public void RemoveCursor(SystmObject systmObject)
    {
        if (!activeCursors.ContainsKey(systmObject))
        {
            return;
        }
        if (storedCursors.Count > 10)
        {
            uiManager.RemovePanel(activeCursors[systmObject].cursor);
        }
        else
        {
            activeCursors[systmObject].cursor.SetActive(false);
            storedCursors.Add(activeCursors[systmObject]);
        }
        activeCursors.Remove(activeCursors[systmObject].obj);
    }

    //  remove all cursors
    public void ClearCursors()
    {
        List<SystmObject> objectCursorIterate = new List<SystmObject>(activeCursors.Keys);

        foreach (SystmObject systmObject in objectCursorIterate)
        {
            uiManager.UnhighlightObject(systmObject);
        }
    }

    //  adjust size of all active cursors
    public void CursorSizeUpdate()
    {
        foreach (ObjectCursor cursor in activeCursors.Values)
        {
            cursor.UpdateCursorSize();
        }
    }

    //  adjust position of all active cursors
    public void CursorPositionUpdate()
    {
        foreach (ObjectCursor cursor in activeCursors.Values)
        {
            cursor.UpdateCursorPosition();
        }
    }
}
