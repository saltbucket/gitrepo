﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//  Issue commands to the fleet
public class CommandMenu : IPlayerInput
{
    public bool active;
    private UIManager uiManager;
    private GameObject parent;
    private GameObject commandButtonPrefab;

    private GameObject attack;
    private GameObject regroup;

    public CommandMenu(UIManager uManager, GameObject prent, GameObject cmmandButtonPrefab)
    {
        uiManager = uManager;
        parent = prent;
        commandButtonPrefab = cmmandButtonPrefab;

        //  attack
        Button attackButton = CreateButton("Attack [c]");
        attackButton.onClick.AddListener(Attack);
        attack = attackButton.gameObject;
        attack.SetActive(false);    //  because it's in CheckPossibleCommands()

        //  regroup
        Button regroupButton = CreateButton("Regroup [f]");
        regroupButton.onClick.AddListener(Regroup);
        regroup = regroupButton.gameObject;
    }

    //  create command button
    private Button CreateButton(string label)
    {
        Button button = uiManager.NewPanel(commandButtonPrefab, parent.transform).GetComponent<Button>();
        button.GetComponentInChildren<Text>().text = label;
        return button;
    }

    //  enter exit toggle
    public void Enter()
    {
        Toggle(true);
        uiManager.player.inputs.Add(this);
    }
    public void Exit()
    {
        uiManager.player.inputs.Remove(this);
        Toggle(false);
    }
    public void Toggle(bool on)
    {
        if (active == on)
        {
            return;
        }

        active = on;
        parent.SetActive(on);
    }
    
    //  enable or disable command buttons
    public void CheckPossibleCommands()
    {
        if (uiManager.membersChosen.Count == 0 || uiManager.membersChosen.Count == 0)
        {
            if (attack.activeInHierarchy)
            {
                attack.SetActive(false);
            }
        }
        else
        {
            if (!attack.activeInHierarchy)
            {
                attack.SetActive(true);
            }
        }
    }

    //  commands
    public void Attack()
    {
        uiManager.player.CommandAttack();
        Exit();
        uiManager.ClearAllChosen();
    }
    public void Regroup()
    {
        uiManager.player.CommandFollowLeader();
        Exit();
        uiManager.ClearAllChosen();
    }

    //  input
    public void InputUpdate()
    {
        CheckPossibleCommands();
        if (Input.GetKeyDown(uiManager.player.settings.escape))
        {
            Exit();
            return;
        }
        
        if (Input.GetKeyDown(KeyCode.C))
        {
            Attack();
        }
        if (Input.GetKeyDown(KeyCode.F))
        {
            Regroup();
        }
    }

    
}
