﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIStyles
{
    //  define styles
    public UIStyle Hospital()
    {
        return new UIStyle
        (
            new Color32(59, 169, 214, 255),
            new Color32(152, 205, 223, 255),
            Resources.Load<Sprite>("SpriteSheets/UI/PanelStyle1"),
            Resources.Load<Sprite>("SpriteSheets/UI/BlueRubber/subPanelMask")
        );
    }

    public UIStyle Sewage()
    {
        return new UIStyle
        (
            new Color32(237, 245, 186, 255),
            new Color32(59, 69, 71, 255),
            Resources.Load<Sprite>("SpriteSheets/UI/PanelStyle2"),
            Resources.Load<Sprite>("SpriteSheets/UI/BlueRubber/subPanelMask")
        );
    }
    
    public UIStyle Purple()
    {
        return new UIStyle
        (
            new Color32(171, 0, 255, 255),
            new Color32(42, 0, 64, 255),
            Resources.Load<Sprite>("SpriteSheets/UI/PanelStyle3"),
            Resources.Load<Sprite>("SpriteSheets/UI/BlueRubber/subPanelMask")
        );
    }

    public UIStyle Acid()
    {
        return new UIStyle
        (
            new Color32(31, 234, 1, 255),
            new Color32(0, 46, 5, 255),
            Resources.Load<Sprite>("SpriteSheets/UI/Acid"),
            Resources.Load<Sprite>("SpriteSheets/UI/BlueRubber/subPanelMask")
        );
    }

    public UIStyle BlueRubber()
    {
        return new UIStyle
        (
            new Color32(17, 205, 251, 255),   //  text
            new Color32(52, 83, 104, 255), //  panel
            Resources.Load<Sprite>("SpriteSheets/UI/BlueRubber/mainPanel"), //  panel
            Resources.Load<Sprite>("SpriteSheets/UI/BlueRubber/subPanel")   //  sub panel

        );
    }

    //  style class
    public class UIStyle
    {
        private Color textColor;
        private Color panelColor;
        private Sprite panelSprite;
        private Sprite subPanelSprite;

        public UIStyle(Color txtColor, Color pnelColor, Sprite pnelSprite, Sprite sbPanelSprite)
        {
            textColor = txtColor;
            panelColor = pnelColor;
            panelSprite = pnelSprite;
            subPanelSprite = sbPanelSprite;
        }

        //  set this style for given component lists
        public void SetStyle(GameObject uiElement)
        {
            switch (uiElement.tag)
            {
                case "uiPanel":
                    SetPanel(uiElement.GetComponent<Image>());
                    break;
                case "uiSubPanel":
                    SetSubPanel(uiElement.GetComponent<Image>());
                    break;
                case "uiText":
                    SetText(uiElement.GetComponent<Text>());
                    break;
                    
                default:
                    Debug.Log("UI element " + uiElement.name + " has no tag");
                    break;
            }
        }

        //  panel
        public void SetPanelStyles(List<Image> panels)
        {
            foreach (Image image in panels)
            {
                SetPanel(image);
            }
        }
        public void SetPanel(Image image)
        {
            image.color = Color.white;
            image.sprite = panelSprite;
        }

        //  sub panel
        public void SetSubPanelStyles(List<Image> subPanels)
        {
            foreach (Image image in subPanels)
            {
                SetSubPanel(image);
            }
        }
        public void SetSubPanel(Image image)
        {
            image.sprite = subPanelSprite;
            image.color = Color.white;
        }

        //  tinted panel
        public void SetTintedPanelStyles(List<Image> tintedPanels)
        {
            foreach (Image image in tintedPanels)
            {
                SetTintedPanel(image);
            }
        }
        public void SetTintedPanel(Image image)
        {
            image.color = panelColor;
        }

        //  text
        public void SetTextStyles(Text[] texts)
        {
            foreach (Text text in texts)
            {
                SetText(text);
            }
        }
        public void SetText(Text text)
        {
            text.color = textColor;
        }
    }
}
