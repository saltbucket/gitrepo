﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class SystmAPI
{
    public static List<Pilot> GetPilots(List<SystmObject> systmObjects)
    {
        List<Pilot> pilots = new List<Pilot>();
        foreach (SystmObject systmObject in systmObjects)
        {
            pilots.Add(systmObject.pilot);
        }
        return pilots;
    }

    public static class Debug
    {
        public static void PrintObjectList(List<SystmObject> objectList)
        {
            foreach (SystmObject systmObject in objectList)
            {
                UnityEngine.Debug.Log(systmObject.name);
            }
        }
    }

    public static class Generic
    {
        public static bool HasActiveChildren(Transform parent)
        {
            if (parent.childCount < 0)
            {
                foreach (Transform child in parent)
                {
                    if (child.gameObject.activeInHierarchy)
                    {
                        return true;
                    }
                }
            }
            return false;          
        }

        public static List<SystmObject> CopyObjectList(List<SystmObject> list)
        {
            List<SystmObject> copy = new List<SystmObject>();
            copy.AddRange(list);
            return copy;
        }

        public static void SetImageAlpha(Image image, float alpha32)
        {
            image.color = new Color(image.color.r, image.color.g, image.color.b, alpha32 / 255);
        }

        public static void SetTextAlpha(Text text, float alpha32)
        {
            text.color = new Color(text.color.r, text.color.g, text.color.b, alpha32 / 255);
        }

        //  child object list comprehension
        public static List<GameObject> GetChildren(Transform parent)
        {
            List<GameObject> children = new List<GameObject>();
            foreach (Transform child in parent)
            {
                children.Add(child.gameObject);
            }
            return children;
        }

        public static Vector2 CanvasOffset()
        {
            return new Vector2(Screen.width / 2, Screen.height / 2);
        }

        //  floats are in range of each other
        public static bool InRange(float valueA, float valueB, float range)
        {
            if (valueA > valueB - range && valueA < valueB + range)
            {
                return true;

            }
            else
            {
                return false;
            }
        }
        // get percentage current where max is 100%
        public static float GetPercentage(float current, float max)
        {
            float onepercent = max / 100;
            return current / onepercent;
        }

        //  loop number
        public static int LoopList(int number, int length)
        {
            if (length == 0)
            {
                UnityEngine.Debug.Log("looped list of length 0");
            }
            if (number < 0)
            {
                return length - 1;
            }
            else if (number > length - 1)
            {
                return 0;
            }
            else return number;
        }

        //  translate percentage to color wher 100% is green and 0% is red
        public static Color PercentageToColor(float percentage)
        {
            float red = 100;
            float green = 100;

            if (percentage <= 50)
            {
                green -= (50 - percentage) * 2;
            }
            else
            {
                red -= (percentage - 50) * 2;
            }

            return new Color(red / 100, green / 100, 0);

        }
        
    }

    public static class Physics
    {
        //  acceleration amount
        public static float Acceleration(Pilot pilot)
        {
            return (pilot.ship.thrust + 1) / pilot.shipRigBody.mass;
        }

        //  acceleration time
        public static float AccelTime(Pilot pilot, float increase)
        {
            return increase / Acceleration(pilot);
        }

        //  acceleration distance
        public static float AccelDistance(Pilot pilot, float increase)
        {
            return ( Mathf.Pow(AccelTime(pilot, increase), 2) * Acceleration(pilot) ) / 2;
        }

        //  turn time
        public static float TurnTime(Pilot pilot, float angle)
        {           
            return (angle / pilot.ship.turn) * Time.deltaTime;
        }

        //  turn distance
        public static float TurnDistance(Pilot pilot, float angle)
        {
            return pilot.shipRigBody.velocity.magnitude* TurnTime(pilot, angle);
        }

        //  distance to change speed
        public static float GetStopDistance(Pilot pilot, float speedChange)
        {
            float total = AccelDistance(pilot, speedChange);
            if (pilot.ship.breaks == 1)
            {
                total += TurnDistance(pilot, 180);
            }
            return total;
        }
    }

    

    public static class Geometry
    {
        public static float WeaponRange(Projectile projectile)
        {
            return projectile.speed * projectile.expire;
        }
        //  rotation of movement direction
        public static Quaternion Forwards(Vector2 velocity)
        {
            return GetDirection(velocity, velocity * -2);
        }

        //  rotation opposite to movement direction
        public static Quaternion Backwards(Vector2 velocity)
        {
            return GetDirection(velocity, velocity * 2);
        }

        //  get direction
        public static Quaternion GetDirection(Vector3 target, Vector3 origin)
        {
            Vector3 diff = target - origin;
            diff.Normalize();

            float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
            return Quaternion.Euler(0f, 0f, rot_z - 90);
        }
        
        //  right is quickest turn direction
        public static bool TurnRight(Quaternion origin, Quaternion target)
        {
            Quaternion relativeToDir = Quaternion.Inverse(origin) * target;
            float facing = relativeToDir.eulerAngles.z;
            if (facing >= 180)
                return true;

            else
                return false;
        }

        //  thrust in this direction to change from current to target velocity
        public static Quaternion GetCorrectiveDirection(Vector2 crrntDirection, Vector2 trgtDirection)
        {
            Vector2 currentDirection = crrntDirection.normalized;
            Vector2 targetDirection = trgtDirection.normalized;

            float angle = Vector2.Angle(currentDirection, targetDirection);
            float offset = angle + ((180 - angle) / 2);
            Quaternion currentRotation = Forwards(currentDirection);
            
            if (TurnRight(currentRotation, Forwards(targetDirection)))
            {
                offset *= -1;
            }

            return currentRotation * Quaternion.Euler(0, 0, offset);
        }

        //  directions are within threshold
        public static bool DirectionsMatch(Vector2 v1, Vector2 v2, float threshold = 0.01f)
        {
            if (Vector2.Distance(v1.normalized, v2.normalized) < threshold)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}

