﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class ResourceManger
{
    private string settingsPath = Application.persistentDataPath + "/Settings/settings.dat";
    private BinaryFormatter formatter = new BinaryFormatter();

    //  write file
    public void WriteBinary(object obj, string path)
    {
        FileStream file = File.Open(path, FileMode.Open);
        formatter.Serialize(file, obj);
    }

    //  read file
    public object ReadBinary(string path)
    {
        if (!File.Exists(path))
        {
            Debug.Log(path + " does not exist!");
            return null;
        }
        FileStream file = File.Open(path, FileMode.Open);
        return formatter.Deserialize(file);
    }

    //  //
    #region Generic
    
    //  list of all file names in directory without extentions
    public List<string> GetFilesNames(string path)
    {
        FileInfo[] info = new DirectoryInfo(path).GetFiles();
        List<string> names = new List<string>();

        for (int i = 0; i < info.Length; i++)
        {
            if (info[i].Name.EndsWith(".prefab"))
            {
                names.Add(Path.GetFileNameWithoutExtension(info[i].Name));
            }
        }

        return names;
    }

    #endregion
    //  //

    //  //
    #region Config
    //  //

    //  save the game settings
    public void SaveGameSettings(Settings settings)
    {
        WriteBinary(settings, settingsPath);
    }

    //  load saved settings
    public Settings LoadGameSettings()
    {
        if (!File.Exists(settingsPath))
        {
            return new Settings();
        }
        else
        {
            return (Settings)ReadBinary(settingsPath);
        }
    }
    
    #endregion

    //  //
    #region Systm
    //  //

    //  random weapon of given faction and class
    public GameObject RandomItem(string type, string faction, int itemClass)
    {
        string path = "_Prefabs/ShipItems/" + faction + "/" + itemClass.ToString() + "/" + type;
        List<string> names = GetFilesNames(Application.dataPath + "/Resources/" + path);
        return Resources.Load<GameObject>(path + "/" + names[Random.Range(0, names.Count - 1)]);
    }

    public GameObject LoadStellarObject(string prefabname)
    {
        return Resources.Load<GameObject>("_Prefabs/Stellar Objects/" + prefabname);
    }
    public GameObject LoadShip(string prefabname)
    {
        return Resources.Load<GameObject>("_Prefabs/Ships/" + prefabname);
    }
    public GameObject LoadPlayerObject(string prefabname)
    {
        return Resources.Load<GameObject>("_Prefabs/Player/" + prefabname);
    }
    public GameObject LoadAI(string prefabname)
    {
        return Resources.Load<GameObject>("_Prefabs/AI/" + prefabname);
    }

    #endregion
}
