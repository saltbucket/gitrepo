﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public ResourceManger resources;
    public MainMenuManager mainMenu;
    public UIManager ui;
    public SystmManager systm;
    public Camera spaceCamera;
    public Settings settings;
    public MiniMap miniMap;
    public PlayerShipController player;
    public bool paused;


    private void SpawnDebugShips()//DEBUG
    {
        for (int i = 0; i < 10; i += 2)
        {
            systm.SpawnOtherShip("Interceptor(2)", "TestAI", new Vector2(i + 2, -2));
        }
    }//DEBUG

    private void Awake()
    {
        resources = new ResourceManger();

        systm.game = this;
        ui.game = this;
        mainMenu.game = this;

        settings = resources.LoadGameSettings();
        paused = false;

        if (!mainMenu.gameObject.activeInHierarchy) //DEBUG
        {
            StartGame();
            ui.tabParent.SetActive(false);
        }//DEBUG
    }

    public void StartGame()
    {
        systm.miniMap = miniMap;
        miniMap.game = this;

        systm.SpawnPlayerShip();

        ui.player = systm.player;

        ui.gameObject.SetActive(true);
        //ui.enabled = true;

        systm.SpawnSystm();
        SpawnDebugShips();
        ui.ChangeStyle(settings.uiStyles.BlueRubber());
    }

    public void Pause(bool pause)
    {
        if (pause && !paused)
        {
            paused = true;
            Time.timeScale = 0;
        }
        else if (!pause && paused)
        {
            paused = false;
            Time.timeScale = 1;
        }
    }

    public void QuitPress()
    {
        Debug.Log("Quit pressed");
        Application.Quit();
    }
}
