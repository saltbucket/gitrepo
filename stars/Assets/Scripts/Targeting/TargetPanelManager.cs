﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetPanelManager
{
    //  UI panels
    private UIManager uiManager;

    public Dictionary<SystmObject, TargetPanel> currentPanels = new Dictionary<SystmObject, TargetPanel>();

    //  active panels
    public List<Dictionary<SystmObject, TargetPanel>> allPanels;
    public Dictionary<SystmObject, TargetPanel> memberPanels = new Dictionary<SystmObject, TargetPanel>();
    public Dictionary<SystmObject, TargetPanel> otherChosenPanels = new Dictionary<SystmObject, TargetPanel>();
    public Dictionary<SystmObject, TargetPanel> fleetTabPanels = new Dictionary<SystmObject, TargetPanel>();

    //  inactive panels
    private List<TargetPanel> storedPanels = new List<TargetPanel>();

    //  constructor
    public TargetPanelManager(UIManager uManager)
    {
        uiManager = uManager;

        allPanels = new List<Dictionary<SystmObject, TargetPanel>>() { memberPanels, otherChosenPanels, fleetTabPanels };
    }

    //  return all active panels
    private List<TargetPanel> GetAllPanels(SystmObject systmObject)
    {
        List<TargetPanel> panels = new List<TargetPanel>();
        foreach (Dictionary<SystmObject, TargetPanel> panelDict in allPanels)
        {
            if (panelDict.ContainsKey(systmObject))
            {
                panels.Add(panelDict[systmObject]);
            }
        }
        return panels;
    }

    //  get panel by SystmObject
    public TargetPanel GetMemberPanel(SystmObject systmObject)
    {
        return memberPanels[systmObject];
    }
    public TargetPanel GetOtherPanel(SystmObject systmObject)
    {
        return otherChosenPanels[systmObject];
    }

    //  clear all panels of type
    public void ClearPanelSet(Dictionary<SystmObject, TargetPanel> panelDict)
    {
        foreach (SystmObject systmObject in panelDict.Keys)
        {
            RemovePanel(systmObject, panelDict);
        }
    }
    
    //  //
    #region Management
    //  //

    //  mouse over panels relating to SystmObject
    public void HighlightPanels(SystmObject systmObject, bool highlight)
    {
        List<TargetPanel> panels = GetAllPanels(systmObject);
        if (allPanels.Count > 0)
        {
            foreach (TargetPanel panel in panels)
            {
                panel.Highlight(highlight);
            }
        }
    }

    //  select panels relating to SystmObject
    public void ChoosePanels(SystmObject systmObject, bool choose)
    {
        List<TargetPanel> panels = GetAllPanels(systmObject);
        if (allPanels.Count > 0)
        {
            foreach (TargetPanel panel in panels)
            {
                panel.Choose(choose);
            }
        }
    }

    //  update all panels relating to SystmObject
    public void UpdatePanels(SystmObject systmObject, bool dead)
    {
        foreach (Dictionary<SystmObject, TargetPanel> panelDict in allPanels)
        {
            if (panelDict.ContainsKey(systmObject))
            {
                if (dead)
                {
                    RemovePanel(systmObject, panelDict);
                }
                else
                {
                    panelDict[systmObject].UpdateTargetStats();
                }
                
            }
        }
    }

    #endregion

    //  //
    #region Recycling
    //  //

    //  get inactive panel or create new panel if none inactive
    public void AddPanel(SystmObject systmObject, Dictionary<SystmObject, TargetPanel> panelDict, GameObject parentPanel)
    {
        if (storedPanels.Count > 0)
        {
            TargetPanel panel = storedPanels[0];
            panelDict[systmObject] = panel;
            storedPanels.Remove(panel);
            panel.transform.SetParent(parentPanel.transform);
            panel.gameObject.SetActive(true);
        }
        else
        {
            panelDict[systmObject] = uiManager.NewPanel(uiManager.smallTargetPanelPrefab, parentPanel.transform).GetComponent<TargetPanel>();
            panelDict[systmObject].ui = uiManager;
        }

        panelDict[systmObject].Construct();
        panelDict[systmObject].ChangeTarget(systmObject);
        systmObject.Browsing(uiManager.player.ship, true);
    }

    //  store active panel or delete if too many stored
    public void RemovePanel(SystmObject systmObject, Dictionary<SystmObject, TargetPanel> panelDict)
    {
        if (!panelDict.ContainsKey(systmObject))
        {
            return;
        }

        if (storedPanels.Count > 10)
        {
            uiManager.RemovePanel(panelDict[systmObject].gameObject);
        }
        else
        {
            panelDict[systmObject].Clear();
            panelDict[systmObject].gameObject.SetActive(false);
            storedPanels.Add(panelDict[systmObject]);
        }
        panelDict.Remove(systmObject);
        systmObject.Browsing(uiManager.player.ship, false);
    }

    #endregion

}
