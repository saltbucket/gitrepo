﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TargetPanel : MonoBehaviour
{
    public UIManager ui;

    private float shieldMin = 125;
    //private float shieldMax = 175;

    public SystmObject target;
    public GameObject shieldIndicator;
    public RectTransform shieldIndicatorRect;
    public GameObject targetImageObject;
    public Image targetImage;
    private Image panelImage;

    //  set image and color
    public void Construct()
    {
        panelImage = gameObject.GetComponent<Image>();
        //UpdateTargetStats();
    }

    //  highlight
    public void Highlight(bool highlight)
    {
        if (highlight)
        {
            SystmAPI.Generic.SetImageAlpha(panelImage, 50);
        }
        else
        {
            SystmAPI.Generic.SetImageAlpha(panelImage, 0);
        }
    }

    //  choose
    public void Choose(bool highlight)
    {
        if (highlight)
        {
            SystmAPI.Generic.SetImageAlpha(panelImage, 100);
        }
        else
        {
            SystmAPI.Generic.SetImageAlpha(panelImage, 0);
        }
    }

    //  change target
    public void ChangeTarget(SystmObject systmObject)
    {
        if (systmObject == target)
        {
            UpdateTargetStats();
        }
        else
        {
            target = systmObject;
            targetImageObject.GetComponent<Image>().overrideSprite = target.uiImage;
            UpdateTargetStats();

            targetImageObject.SetActive(true);
        }  
    }

    //  target object variables changed
    public void UpdateTargetStats()
    {
        if (target.disabled)
        {
            //  filter and drop if necessary (disabled are non-aggressive)
        }
        if (target.shieldHP <= 0)
        {
            shieldIndicator.SetActive(false);
        }
        else
        {
            shieldIndicator.SetActive(true);
        }

        //  translate percentage shields to size of indicator
        float percentage = SystmAPI.Generic.GetPercentage(target.shieldHP, target.maxShields) / 2;
        float indicatorSize = ((percentage / 2) + shieldMin);

        shieldIndicatorRect.sizeDelta = new Vector2(indicatorSize, indicatorSize);

        //  translate percentage health to image color

        if (target.healthHP == target.maxHealth)
        {
            targetImage.color = ui.game.settings.colorGreen;
        }
        else if (target.disabled)
        {
            targetImage.color = ui.game.settings.colorGrey;
        }
        else if (target.healthHP != target.maxHealth)
        {
            targetImage.color = SystmAPI.Generic.PercentageToColor
            (
            SystmAPI.Generic.GetPercentage(target.healthHP, target.maxHealth)
            );
        }      
    }

    //  clear this panel
    public void Clear()
    {
        target = null;
        shieldIndicator.SetActive(false);
        targetImageObject.SetActive(false);
    }
}
