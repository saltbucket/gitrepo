﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TargetBrowser : IPlayerInput
{
    private UIManager uiManager;
    
    //  UI panels
    private Text title;
    private Text tooltip;
    private Text primary;
    private Text secondary;
    public TargetPanel browserPanel;
    private GameObject filterPanel;
    private GameObject[] filterButtons;

    //  default filter settings
    public bool[] targetingFilters = new bool[]
    {
        true,//ships
        true,//panets
        true,//stations
        true,//aggressive
        true//targeting
    };
    
    public SystmObject currentTarget;

    //  //
    #region Generic
    //  //

        //  constructor
    public TargetBrowser(UIManager uManager, Text ttle, Text ttip, Text pmary, Text scndary, GameObject fPanel)
    {
        uiManager = uManager;
        title = ttle;
        tooltip = ttip;
        primary = pmary;
        secondary = scndary;
        filterPanel = fPanel;

        browserPanel = filterPanel.transform.parent.gameObject.GetComponent<TargetPanel>();
        filterButtons = SystmAPI.Generic.GetChildren(filterPanel.transform).ToArray();

        //  add input
        uiManager.player.inputs.Add(this);
    }

    //  clear browser panel
    public void ClearBrowser()
    {
        if (currentTarget != null)
        {
            if (!uiManager.IsChosen(currentTarget))
            {
                uiManager.UnhighlightObject(currentTarget);
            }
            currentTarget = null;
            browserPanel.Clear();
            title.text = "No Target";
            tooltip.text = "";
            primary.text = "";
            secondary.text = "";
        }
    }
    
    //  update browser panel with given systemObject
    public void RefreshBrowser(SystmObject sysObj)
    {
        title.text = sysObj.tooltipTitle;
        tooltip.text = sysObj.tooltipText;

        //  primary weapon display
        string primWeaps = "";
        foreach (SystmObject.ShipWeapSlot weapSlot in sysObj.weapSlots_Primary)
        {
            /*if (weapSlot.weap == null)
            {
                continue;
            }
            primWeaps += weapSlot.weap.GetComponent<ItemBehaviour>().itemClass.ToString();
            primWeaps += "\n";*/
        }
        
        primary.text = primWeaps;

        browserPanel.ChangeTarget(sysObj);
    }

    #endregion

    //  //
    #region Filter 
    //  //

    //  return true if target passes all filters
    protected bool FilterTarget(SystmObject systmObject)
    {
        if (systmObject.landable)
        {
            return true;
        }
        if (uiManager.IsChosen(systmObject) || uiManager.player.ship == systmObject)
        {
            return false;
        }

        else if (uiManager.fleetPanel.active)
        {
            return true;
        }
        else if (systmObject.HasCommander(uiManager.player.ship))
        {
            return false;
        }        
        
        return true;
    }

    #endregion

    //  //
    #region Browsing
    //  //

    public SystmObject BrowseCurrent(bool forward)
    {
        return BrowseTargets(forward, uiManager.currentBrowsing); 
    }

    //browse all object in given list
    public SystmObject BrowseTargets(bool forward, List<SystmObject> sysObjects)
    {
        int currentIndex;

        if (sysObjects.Count == 0)
        {
            return null;
        }
        if (!sysObjects.Contains(currentTarget))
        {
            foreach (SystmObject systmObject in sysObjects)
            {
                if (FilterTarget(systmObject))
                {
                    ChangeCurrentTarget(systmObject);
                    return currentTarget;
                }
            }
            return null;
        }
        else
        {
            //find index of gameobject with target's instance ID
            var predicate = new SystmManager.GetSystmObjectIndex(currentTarget.gameObject.GetInstanceID());
            currentIndex = sysObjects.FindIndex(predicate.HasObjectID);
        }
        
        
        //switch target from current index
        if (forward)
        {
            //forwards
            for (int i = 0; i < sysObjects.Count; i++)
            {
                currentIndex += 1;
                if (currentIndex > sysObjects.Count - 1)
                    currentIndex = 0;

                if (FilterTarget(sysObjects[currentIndex]))
                {
                    ChangeCurrentTarget(sysObjects[currentIndex]);
                    return currentTarget;
                }

            }
            ClearBrowser();
            return null;
        }
        else
        {
            //backwards
            for (int i = 0; i < sysObjects.Count; i++)
            {
                currentIndex -= 1;
                if (currentIndex < 0)
                    currentIndex = sysObjects.Count - 1;

                if (FilterTarget(sysObjects[currentIndex]))
                {
                    ChangeCurrentTarget(sysObjects[currentIndex]);
                    return currentTarget;
                }
            }
            ClearBrowser();
            return null;
        }
    }
    #endregion

    //  handle new target
    public void ChangeCurrentTarget(SystmObject newTarget)
    {
        if (newTarget == null || newTarget == currentTarget)
        {
            return;
        }
        if (currentTarget != null && !uiManager.IsChosen(currentTarget))// move back to browse method
        {
            uiManager.UnhighlightObject(currentTarget);
            currentTarget.Browsing(uiManager.player.ship, false);
        }
        currentTarget = newTarget;
        currentTarget.Browsing(uiManager.player.ship, true);
        uiManager.HighlightObject(currentTarget);
        RefreshBrowser(currentTarget);
    }

    //  //
    #region Input Update 
    //  //

    public void InputUpdate()
    {
        //browse targets
        if (Input.GetKeyDown(uiManager.player.settings.ship_BrowseTargets))
        {
            if (Input.GetKey(uiManager.player.settings.alternate))
            {
                BrowseCurrent(false);
            }
            else
            {
                BrowseCurrent(true);
            }
        }        
    }

    #endregion
}