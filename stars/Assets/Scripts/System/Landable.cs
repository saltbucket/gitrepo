﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Landable : SystmObject
{
    [Serializable]
    public struct Specs
    {
        public Specs
        (
            bool _outfit,
            bool _shipyard,
            bool _bar,

            int _maxOutfitClass,
            string[] _outfitManufacturers
        )
        {
            outfit = _outfit;
            shipyard = _shipyard;
            bar = _bar;

            maxOutfitClass = _maxOutfitClass;
            outfitManufacturers = _outfitManufacturers;
        }

        public bool outfit;
        public bool shipyard;
        public bool bar;

        public int maxOutfitClass;
        public string[] outfitManufacturers;
    };

    public Specs specs;

    private void Awake()
    {
        landable = true;
        if (pilot == null)
        {
            pilot = new Pilot();
        }
    }

    public void Land(SystmObject systmObject)
    {
        Debug.Log(systmObject.name + " has landed on " + gameObject.name);
    }
}
