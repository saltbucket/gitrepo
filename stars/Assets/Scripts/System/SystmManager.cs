﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SystmManager : MonoBehaviour
{

    [Header("Systm")]
    public GameManager game;
    public PlayerShipController player;
    public MiniMap miniMap;
    public bool active = false;

    [Header("Other")]
    public List<SystmObject> systmObjects = new List<SystmObject>();
    public List<SystmObject> landables = new List<SystmObject>();
    public List<SystmObject> ships = new List<SystmObject>();

    public SystmLibrary systmLibrary;

    int sortingOrderNumber;

    //  //
    #region Generic
    //  //

    protected void Awake()
    {
        systmLibrary = new SystmLibrary();
        systmLibrary.SetSystm();
        slowUpdate = Time.fixedTime + 0.1f;
        slowerUpdate = Time.fixedTime + 0.5f;
    }

    #endregion

    //  //
    #region SystmObject Management
    //  //

    IEnumerator DestroyAtEndOfFrame(SystmObject systmObject)
    {
        yield return new WaitForEndOfFrame();
        Destroy(systmObject.gameObject);
    }
    private void DestroyObject(SystmObject systmObject)
    {
        StartCoroutine(DestroyAtEndOfFrame(systmObject));
    }

    //  add object to system list and minimap
    public void AddToSystm(GameObject newObject)
    {
        SystmObject systmObject = newObject.GetComponent<SystmObject>();
        if (systmObject == null)
        {
            Debug.Log("Tried to add " + newObject.name + " to systmObjects with no SystmObject component attached");
            return;
        }

        systmObjects.Add(systmObject);
        if (systmObject.landable)
        {
            landables.Add(systmObject);
        }
        if (systmObject.tag == "ship" || systmObject.tag == "DEBUGOBJ")
        {
            ships.Add(systmObject);
        }

        systmObject.systm = this;

        systmObject.imageObject.GetComponent<SpriteRenderer>().sortingOrder = sortingOrderNumber;
        sortingOrderNumber += 1;

        if (miniMap.isActiveAndEnabled == true)
        {
            systmObject.mmObject = miniMap.NewMinimapObj(systmObject.gameObject);
        }

        if (!systmObject.gameObject.activeInHierarchy)
        {
            systmObject.gameObject.SetActive(true);
        }
    }

    //remove object from system list and minimap
    public void RemoveFromSystm(SystmObject systmObject, MiniMap.MinimapObject mmObject)
    {
        landables.Remove(systmObject);
        ships.Remove(systmObject);

        systmObjects.Remove(systmObject);
        miniMap.DestroyMinimapObj(mmObject);
        DestroyObject(systmObject);
    }

    public void RefreshInSystm(SystmObject systmObject)
    {
        miniMap.RefreshMinimapObj(systmObject);
    }
    
    //  predicate for getting list index by instance ID
    public class GetSystmObjectIndex
    {
        int gameObjectID;
        public GetSystmObjectIndex(int gObjID)
        {
            gameObjectID = gObjID;
        }
        public bool HasObjectID(SystmObject obj)
        {
            return (gameObjectID == obj.gameObject.GetInstanceID());
        }
    }

    #endregion

    //  //
    #region Custom Update
            //  //

    float slowUpdate;
    float slowerUpdate;

    //  counter
    private void FixedUpdate()
    {
        if (!active)
            return;

        if (Time.fixedTime >= slowUpdate)
        {
            miniMap.RefreshPositions();
            SlowUpdate();
            slowUpdate = Time.fixedTime + 0.1f;
        }
        if (Time.fixedTime >= slowerUpdate)
        {
            SlowerUpdate();
            slowerUpdate = Time.fixedTime + 0.5f;
        }
    }

    //  slow
    private void SlowUpdate()
    {
        if (!active)
            return;

        foreach (SystmObject systmObject in systmObjects)
        {
            if (systmObject.pilot != null)
            {
                systmObject.pilot.SlowUpdate();
            }
        }
    }

    //  slower
    private void SlowerUpdate()
    {
        if (!active)
            return;

        foreach (SystmObject systmObject in systmObjects)
        {
            if (systmObject.pilot != null)
            {
                systmObject.pilot.SlowerUpdate();
            }
        }
    }

    #endregion

    //  //
    #region Spawning
            //  //

    public void SpawnPlayerShip(Vector2 position = new Vector2())
    {
        // ship object
        GameObject ship = Instantiate(game.resources.LoadShip("Interceptor(2)"));
        ship.transform.position = position;
        game.spaceCamera.transform.position = new Vector3(ship.transform.position.x, ship.transform.position.y, game.spaceCamera.transform.position.z);

        //  player ship controller
        player = Instantiate(game.resources.LoadPlayerObject("Player"), ship.transform).GetComponentInChildren<PlayerShipController>();
        player.transform.position = ship.transform.position;
        Debug.Log("game");
        player.game = game;
        player.spaceCamera = game.spaceCamera;
        player.ui = game.ui;
        player.shipFollower = Instantiate(game.resources.LoadPlayerObject("Ship Follower"));
        
        //  autopilot AI
        player.autopilotAI = Instantiate(game.resources.LoadPlayerObject("Autopilot AI"), ship.transform).GetComponent<AIShipController>();
        player.autopilotAI.GetShipComponents();

        game.player = player;
        
        ship.SetActive(true);
        AddToSystm(ship);
    }

    public AIShipController SpawnOtherShip(string shipName, string ai, Vector2 position)
    {
        GameObject ship = Instantiate(game.resources.LoadShip(shipName));
        ship.transform.position = position;
        ship.SetActive(true);
        AddToSystm(ship);
        return Instantiate(game.resources.LoadAI(ai), position, new Quaternion(), ship.transform).GetComponent<AIShipController>();
    }

    public void SpawnSystm()
    {
        SpawnPlanets();
        active = true;
    }

    private void SpawnPlanets()
    {
        foreach(string name in systmLibrary.current.landables)
        {
            GameObject prefab = game.resources.LoadStellarObject(name);
            Landable landable = prefab.GetComponent<Landable>();
            AddToSystm(Instantiate(prefab, landable.transform.position, new Quaternion()));
        }
    }

    

    #endregion

}
