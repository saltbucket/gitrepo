﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SystmObject : MonoBehaviour
{
    [Header("SystmObject")]
    public SystmManager systm;
    public Pilot pilot;
    public bool landable;

    [Header("UI")]
    public GameObject imageObject;
    public Sprite uiImage;

    public string tooltipTitle;
    public string tooltipText;

    [HideInInspector]
    public MiniMap.MinimapObject mmObject;
    [HideInInspector]
    public Bounds bounds;

    [Header("Stats")]
    public int maxShields;
    public int maxHealth;
    public int armor;
    public bool invulnerable;
    public int shipClass;

    [Header("Variables")]
    public float shieldHP;
    public float healthHP;
    public bool disabled;


    [Header("- Weapons")]

    //  weapon/hangar slots by type
    public ShipWeapSlot[] weapSlots_Hangar;
    public ShipWeapSlot[] weapSlots_Primary;
    public ShipWeapSlot[] weapSlots_Secondary;

    //  all active turret slots
    public List<ShipWeapSlot> allTurrets = new List<ShipWeapSlot>();

    //  all enemies in range of turrets
    public List<SystmObject> turretTargets = new List<SystmObject>();
    private float maxTurretRange;

    //  toggle between auto/manual turret targeting
    public bool turretsTargetMouse;

    private bool primaryIsFiring;
    private bool secondaryIsFiring;

    //  targeting
    [HideInInspector]
    public SystmObject target = null;
    //  other ships that are browsing this
    [HideInInspector]
    private List<SystmObject> othersBrowsing = new List<SystmObject>();
    //  other ships that have their target locked on this
    [HideInInspector]
    private List<SystmObject> othersLocked = new List<SystmObject>();

    //  //
    #region Generic
    //  //

    private void Start()
    {
        //  get bounds component for UI overlay resizing
        bounds = imageObject.GetComponent<SpriteRenderer>().bounds;
    }

    //  assign new pilot, place in ship
    public void RefreshPilot(Pilot plt)
    {
        pilot = plt;
        pilot.transform.position = transform.position;
    }

    //  check if given systmObject has current pilot in list of enemies
    public bool EnemyOf(SystmObject systmObject)
    {
        if (systmObject.pilot == null)
        {
            return false;
        }
        if (systmObject.pilot.enemies.Contains(pilot))
        {
            return true;
        }
        else return false;

    }

    //  destroy game object and remove from systm
    public void DestroySystmObject()
    {
        if (pilot.FleetMember())
        {
            pilot.fleet.RemoveMember(pilot);
        }
        systm.RemoveFromSystm(this, mmObject);
    }

    // disable object
    public void DisableObject()
    {
        if (disabled)
            return;

        disabled = true;
        systm.RefreshInSystm(this);
    }

    #endregion

    //  //
    #region Targeting
    //  //

    //  add to target locked list, tell targeted pilot it has been targeted
    public void SystmObjectTargetedBy(SystmObject systmObject, bool targeted)
    {
        //  tell commander something
        if (pilot.FleetMember())
        {
            //  commander.membertargeted
        }

        //  tell pilot it's ship has been targeted
        if (pilot != null)
        {
            pilot.TargetedBy(systmObject, targeted);
        }

        //  track in othersLocked
        Locked(systmObject, targeted);
    }

    //  add/remove from othersLocked
    public void Locked(SystmObject systmObject, bool add)
    {
        if (add && !othersLocked.Contains(systmObject))
        {
            othersLocked.Add(systmObject);
        }
        else if (!add && othersLocked.Contains(systmObject))
        {
            othersLocked.Remove(systmObject);
        }
    }

    //  add/remove from othersBrowsing
    public void Browsing(SystmObject systmObject, bool add)
    {
        if (add && !othersBrowsing.Contains(systmObject))
        {
            othersBrowsing.Add(systmObject);
        }
        else if (!add && othersBrowsing.Contains(systmObject))
        {
            othersBrowsing.Remove(systmObject);
        }
    }

    //  hit by weapon/other
    public void Hit(SystmObject attacker, Projectile proj)
    {
        //  invulnerability
        if (invulnerable)
            return;

        //  disabled when below -(25% maxHP)
        float disableRange = -(maxHealth / 4);

        //  damage shields if active
        if (shieldHP >= 1)
        {
            shieldHP -= proj.projDamage;
        }
        else
        {
            shieldHP = 0;
            healthHP -= proj.projDamage - armor;

            //  if below -(25% maxHP), destroy and return
            if (healthHP <= disableRange)
            {
                RefreshTargeters(true);
                RefreshCommander(true);
                DestroySystmObject();
                return;
            }
            //  if less than 0 HP but not dead, disable
            else if (healthHP > disableRange && healthHP < 0)
            {
                DisableObject();
            }
        }
        //  update associated ships with change in HP
        RefreshCommander();
        RefreshTargeters();
    }

    //  update commander with change
    private void RefreshCommander(bool dead = false)
    {
        if (pilot.FleetMember())
        {
            pilot.fleet.commander.UpdateMember(this, dead);
        }
    }

    //  update targeting ships with change
    private void RefreshTargeters(bool dead = false)
    {
        List<SystmObject> browsingCopy = new List<SystmObject>(othersBrowsing);
        List<SystmObject> lockedCopy = new List<SystmObject>(othersLocked);

        foreach (SystmObject systmObject in SystmAPI.Generic.CopyObjectList(othersBrowsing))
        {
            systmObject.pilot.UpdateTarget(this, dead);
        }
        foreach (SystmObject systmObject in SystmAPI.Generic.CopyObjectList(othersLocked))
        {
            systmObject.pilot.UpdateTarget(this, dead);
        }
    }

    #endregion

    //  //
    #region Weapons
    //  //
    
    //  get all targets in turret range, runs twice per second
    //  TODO: find a way to do this without using GetComponent<>()?
    private void GetTurretTargets()
    {
        Collider2D[] inRange = Physics2D.OverlapCircleAll(transform.position, maxTurretRange);

        turretTargets.Clear();
        
        for (int i = 0; i < inRange.Length; i++)
        {
            SystmObject systmObject = inRange[i].GetComponent<SystmObject>();

            if (systmObject != null && EnemyOf(systmObject))
            {
                turretTargets.Add(systmObject);
            }
        }
    }
    
    /*private List<ShipWeapSlot> GetTurretSlots(ShipWeapSlot[] allWeapSlots)
    {
        List<ShipWeapSlot> turretWeapSlots = new List<ShipWeapSlot>();
        for (int i = 0; i < allWeapSlots.Length; i++)
        {
            if (allWeapSlots[i].isTurret)
            {
                turretWeapSlots.Add(weapSlots_Primary[i]);
            }
        }
        return turretWeapSlots;
    }*/

    //  turn turrets towards position
    private void TurnTurrets(ShipWeapSlot[] slots, Vector2 targetPosition)
    {
        for (int i = 0; i < slots.Length; i++)
        {
            if (slots[i].isTurret)
            {
                slots[i].FacePosition(targetPosition);
            }
        }
    }

    //  TODO:   Move this out of SystmObject?
    //          Separate into base cannon class and inhereting turret/hangar classes
    //          
    //  //
    #region Weapon Slot Class
    //  //

    [Serializable]
    public class ShipWeapSlot
    {
        public GameObject parent;
        public int maxClass;
        public Vector2 slotPos;
        public GameObject[] subSlots;
        public bool onRightHandSide;

        [NonSerialized]
        public ShipWeapon[] weaps;

        public float range;

        //  turret
        public bool isTurret;
        public int turn;
        public List<SystmObject> turretTargets;
        public SystmObject currentTarget;

        //  construct
        public void InitialiseTurret(List<SystmObject> tTargets)
        {
            turretTargets = tTargets;
        }

        //  left
        public void Left()
        {
            parent.transform.Rotate(Vector3.forward * turn);
        }
        //  right
        public void Right()
        {
            parent.transform.Rotate(-Vector3.forward * turn);
        }

        //  face to front of ship
        public void FaceForwards()
        {
            if (parent.transform.localEulerAngles != Vector3.zero)
            {
                FaceDirection(Quaternion.Euler(Vector3.zero));
            }
        }

        //  face vector2
        public bool FacePosition(Vector2 position)
        {
            Quaternion direction = SystmAPI.Geometry.GetDirection(position, parent.transform.position);
            return FaceDirection(direction);
        }

        //  face current target
        public bool FaceTarget()
        {
            if (!TargetEligible())
            {
               currentTarget = turretTargets[UnityEngine.Random.Range(0, turretTargets.Count - 1)];
            }
            if (currentTarget == null)
            {
                return false;
            }
            Quaternion direction = SystmAPI.Geometry.GetDirection(currentTarget.transform.position, parent.transform.position);
            return FaceDirection(direction);
        }

        //  face given direction
        public bool FaceDirection(Quaternion dir)
        {
            //Rotation relative to desired direction
            if (!SystmAPI.Generic.InRange(Quaternion.Angle(parent.transform.rotation, dir), 0, turn))
            {
                if (SystmAPI.Geometry.TurnRight(parent.transform.rotation, dir))
                    Right();
                else
                    Left();
                return false;
            }
            else
            {
                if (parent.transform.rotation != dir)
                    parent.transform.rotation = dir;
                return true;
            }
        }

        //  check if target alive and in range
        private bool TargetEligible()
        {
            if (currentTarget == null)
            {
                return false;
            }
            else if (Vector2.Distance(parent.transform.position, currentTarget.transform.position) <= range)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        
    }

    #endregion

    //  assign all weapons on ship
    public void RefreshWeapons()
    {
        allTurrets.Clear();
        AssignWeaponObjects(weapSlots_Hangar);
        AssignWeaponObjects(weapSlots_Primary);
        AssignWeaponObjects(weapSlots_Secondary);
    }

    //  get weapon object, assign weapon script component/owner/slot
    private void AssignWeaponObjects(ShipWeapSlot[] slotArray)
    {
        for (int i = 0; i < slotArray.Length; i++)
        {
            ShipWeapSlot weapSlot = slotArray[i];
            weapSlot.weaps = new ShipWeapon[weapSlot.subSlots.Length];
            for (int e = 0; e < weapSlot.subSlots.Length; e++)
            {
                if (weapSlot.subSlots[e].transform.childCount > 0)
                {
                    weapSlot.weaps[e] = weapSlot.subSlots[e].GetComponentInChildren<ShipWeapon>();
                    weapSlot.weaps[e].Assign(this, weapSlot);
                }
                else
                {
                    continue;
                }
            }

            //  handle turret slots, get max turret range and add to turrets list
            if (weapSlot.isTurret && weapSlot.weaps.Length > 0)
            {
                weapSlot.InitialiseTurret(turretTargets);
                float range = SystmAPI.Geometry.WeaponRange(weapSlot.weaps[0].currentProjectile);
                if (range > maxTurretRange)
                {
                    maxTurretRange = range;
                }
                weapSlot.range = range;
                allTurrets.Add(weapSlot);
            }
        }
    }

    //  //
    #region Fire
    //  //

    //  broken way to delay firing
    //  TODO: fix this so you can't just spam the key
    IEnumerator WaitThenFire(ShipWeapon weapon, float wait)
    {
        yield return new WaitForSeconds(wait);
        //  after waiting, deactivate and store
        if (gameObject.activeInHierarchy == true)
        {
            weapon.FireDown();
        }
    }

    //  stop all weapons
    public void StopWeapons()
    {
        FirePrimaryUp();
        FireSecondaryUp();
    }


    public void FireWeapons(ShipWeapSlot[] slotset, bool down)
    {
        //  for every weapon slot of slot type
        for (int i = 0; i < slotset.Length; i++)
        {
            for (int e = 0; e < slotset[i].weaps.Length; e++)
            {
                //  fire
                if (down)
                {
                    float stagger = CalculateStaggerTime(slotset, i);
                    //  fire weapons, delay firing based on index * stagger
                    //  if only 1 weapon, stagger is always 0
                    StartCoroutine(WaitThenFire(slotset[i].weaps[e], stagger * (e + 1)));
                    //slotset[i].weaps[e].FireDown();
                }
                //  stop firing
                else
                {
                    slotset[i].weaps[e].FireUp();
                    slotset[i].weaps[e].DumpStoredProj();
                }
            }
        }
    }

    //  caculate time to stagger weapon fire making multiple weapons shoot in succession
    private float CalculateStaggerTime(ShipWeapSlot[] slotset, int index)
    {
        int length = slotset[index].weaps.Length;
        if (length > 1)
        {
            return slotset[index].weaps[0].fireRate / length;
        }
        else
        {
            return 0;
        }
    }

    //  primary down
    public void FirePrimaryDown()
    {
        if (!primaryIsFiring)
        {
            FireWeapons(weapSlots_Primary, true);
            primaryIsFiring = true;
        }

    }

    //  primary up
    public void FirePrimaryUp()
    {
        //  TODO: is this  doing anything?
        if (this == null)
        {
            return;
        }
        if (primaryIsFiring)
        {
            FireWeapons(weapSlots_Primary, false);
            primaryIsFiring = false;
        }

    }

    //  secondary down
    public void FireSecondaryDown()
    {
        if (!secondaryIsFiring)
        {
            FireWeapons(weapSlots_Secondary, true);
            secondaryIsFiring = true;
        }
    }

    // secondary up
    public void FireSecondaryUp()
    {
        if (secondaryIsFiring)
        {
            FireWeapons(weapSlots_Secondary, false);
            secondaryIsFiring = false;
        }
    }
    
    public void DisableWeapons()
    {

    }
    #endregion

    #endregion


    //  //
    #region Updates
    //  //

    //  turn turrets to face target/forwards/cursor
    public void AIFixedUpdate()
    {
        if (turretsTargetMouse)
        {
            TurnTurrets(allTurrets.ToArray(), pilot.MousePosition());
        }
        else if (turretTargets.Count > 0)
        {
            for (int i = 0; i < allTurrets.Count; i++)
            {
                allTurrets[i].FaceTarget();
            }
        }
        else
        {
            for (int i = 0; i < allTurrets.Count; i++)
            {
                allTurrets[i].FaceForwards();
            }

        }
    }

    public void SlowUpdate()
    {
    }

    //  update list of targets in range of turrets
    public void SlowerUpdate()
    {
        if (!turretsTargetMouse)
        {
            GetTurretTargets();
        }
        
    }

    #endregion
    //  //
    #region Fleet
    //  //
    
    //  check if this ship is commanded by SystmObject commander
    public bool HasCommander(SystmObject commander)
    {
        if (pilot == null)
        {
            return false;
        }
        if (pilot.FleetMember() && pilot.fleet.commander == commander.pilot)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public int Group()
    {
        return pilot.groupNumber;
    }

    #endregion
    
}