﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SystmLibrary
{
    public Systm current;

    public Dictionary<string, Systm> systms = new Dictionary<string, Systm>()
    {
        {
            "Sol",  //  name
            new Systm(
                Vector2.zero,   //  position
                new string[]    //  landables
                {
                    "TestPlanet"
                })
        }
    };

    //  change current system
    public void SetSystm()//DEBUG
    {
        current = systms["Sol"];
    }

    //  system specs
    public struct Systm
    {
        //  position in map
        Vector2 position;
        //  landable object prefab names
        public string[] landables;

        public Systm(Vector2 _position, string[] _landables)
        {
            position = _position;
            landables = _landables;
        }
    }

    
}

