﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FleetTab : Tab
{
    public GameObject memberPrefab;
    public GameObject groupsParent;
    public TargetPanelManager targetPanels;

    public Dictionary<SystmObject, FleetTabMember> allMembers = new Dictionary<SystmObject, FleetTabMember>();
    public List<SystmObject> selected = new List<SystmObject>();

    private Fleet fleet;
    private List<Transform> groups = new List<Transform>();
    
    //  enter
    public override void Enter()
    {
        base.Enter();
        foreach (Pilot member in fleet.AllMembers())
        {
            manager.game.ui.UpdateTargetStats(member.ship, false);
        }
    }

    //  exit
    public override void Exit()
    {
        base.Exit();
        DeselectAll();
    }

    //  get groups and target panel manager
    public override void Construct()
    {
        fleet = manager.game.player.fleet;
        targetPanels = manager.game.ui.targetPanels;
        groups.Clear();
        foreach (Transform child in groupsParent.transform)
        {
            groups.Add(child);
            if (child.childCount == 0)
            {
                child.gameObject.SetActive(false);
            }
        }
    }

    //  when member is added
    private void CheckActive(int group)
    {
        if (!groups[group].gameObject.activeInHierarchy)
        {
            groups[group].gameObject.SetActive(true);
        }
    }

    //  when member is removed
    private void CheckEmpty(SystmObject member, int modify = 0)
    {
        int group = member.Group();
        if (groups[group].childCount == modify)
        {
            groups[group].gameObject.SetActive(false);
        }
    }

    //  add member
    public void AddMember(SystmObject member, int group)
    {
        CheckActive(group);
        FleetTabMember memberPanel = Instantiate(memberPrefab, groups[group]).GetComponent<FleetTabMember>();
        //memberPanel.tag = "uiSubPanel";
        memberPanel.Refresh(member);
        memberPanel.tab = this;

        targetPanels.AddPanel(member, targetPanels.fleetTabPanels, memberPanel.gameObject);
        allMembers[member] = memberPanel;
    }

    //  remove member
    public void RemoveMember(SystmObject member)
    {
        Destroy(allMembers[member].gameObject);
        allMembers.Remove(member);
        CheckEmpty(member);
    }

    //  member panel clicked
    public void RightClicked(FleetTabMember member)
    {
        if (!selected.Contains(member.systmObject))
        {
            Select(member);
        }
        else
        {
            Deselect(member);
        }
    }

    //  select panel
    private void Select(FleetTabMember member)
    {
        Debug.Log("Selecting member");
        SystmAPI.Generic.SetImageAlpha(member.image, 10);
        selected.Add(member.systmObject);
    }

    //  deselect panel
    private void Deselect(FleetTabMember member)
    {
        Debug.Log("deselecting member");
        SystmAPI.Generic.SetImageAlpha(member.image, 0);
        selected.Remove(member.systmObject);
    }

    //  deselect all
    private void DeselectAll()
    {
        foreach (SystmObject member in SystmAPI.Generic.CopyObjectList(selected))
        {
            Deselect(allMembers[member]);
        }
        selected.Clear();
    }

    //  select group
    private void SelectGroup(int group)
    {
        foreach (SystmObject member in manager.game.player.fleet.GroupShips(group))
        {
            Select(allMembers[member]);
        }
    }

    //  alphanumeric key pressed
    public void GroupInput(int group)
    {
        if (selected.Count == 0)
        {
            SelectGroup(group);
            return;
        }
        else
        {
            MoveSelected(group);
        }
    }

    //  move selected
    public void MoveSelected(int group)
    {
        CheckActive(group);
        foreach (SystmObject member in selected)
        {
            CheckEmpty(member, 1);
            MoveFleetMember(member, group);
        }
        DeselectAll();
    }

    //  change member to group
    public void MoveFleetMember(SystmObject member, int group)
    {
        fleet.RemoveMember(member.pilot);
        fleet.AddMember(member.pilot, group);

        manager.game.ui.fleetPanel.UpdateMemberConfig(member);
        allMembers[member].transform.parent = groups[group];
    }

    //  input
    protected override void Escape()
    {
        
        
        if (selected.Count > 0)
        {
            DeselectAll();
        }
        else
        {
            base.Escape();
        }
        
    }
}
