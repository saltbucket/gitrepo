﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShipTab : Tab
{
    [Header("Ship Tab")]
    public GameObject stats;

    //player
    private PlayerShipController player;

    //  stats
    private Text shieldHP;
    private Text shipHP;
    private Text speed;
    private Text turn;
    private Text thrust;

    //  awake
    private void Awake()
    {
        GetTextObjects();
    }

    //  enter
    public override void Enter()
    {
        base.Enter();
        RefreshStats();
    }
    
    //  assign Text components to array by order in hierarchy
    private void GetTextObjects()
    {
        Text[] textObjects = stats.GetComponentsInChildren<Text>();

        shieldHP = textObjects[0];
        shipHP = textObjects[1];
        speed= textObjects[2];
        turn= textObjects[3];
        thrust = textObjects[4];
    }

    //  update text components with ship stats
    public void RefreshStats()
    {
        player = manager.game.systm.player;
        heading.text = player.ship.tooltipTitle + "\n(player)";

        shieldHP.text = "Shields: " + player.ship.maxShields + " | " + player.ship.shieldHP;
        shipHP.text = "Structure: " + player.ship.maxHealth + " | " + player.ship.healthHP;
        speed.text = "Speed: " + player.ship.speed;
        turn.text = "Turn: " + player.ship.turn;
        thrust.text = "Thrust: " + player.ship.thrust;
    }

}
