﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutfitTab : Tab
{
    public GameObject inventoryParent;
    public GameObject itemPanelPrefab;
    public Inventory inventory;

    public override void Construct()
    {
        inventory = new Inventory(inventoryParent.transform, manager.game, itemPanelPrefab);
    }

    //  menu opened
    public override void OnMenuOpen()
    {
        if (manager.landable != null && manager.landable.specs.outfit)
        {
            FillInventory();
            Activate();
        }
    }

    //  menu closed
    public override void OnMenuClose()
    {
        Deactivate();
    }

    //  enter
    public override void Enter()
    {
        base.Enter();
    }

    //  exit
    public override void Exit()
    {
        base.Exit();
    }

    //  generate items for sale
    private void FillInventory()
    {
        //  get some random items
        for (int i = 0; i < 5; i++) //DEBUG
        {
            inventory.AddItem(manager.game.resources.RandomItem("weapon", "neutral", 1), Random.Range(1,10));
            inventory.AddItem(manager.game.resources.RandomItem("ammo", "neutral", 1), Random.Range(1, 10));

        }
    }
}
