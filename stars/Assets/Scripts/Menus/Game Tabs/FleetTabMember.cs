﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class FleetTabMember : MonoBehaviour, IPointerClickHandler
{
    public SystmObject systmObject;
    public GameObject details;
    public FleetTab tab;

    public Text title;
    public Text text;
    public Image image;

    private RectTransform rect;

    private void Awake()
    {
        rect = GetComponent<RectTransform>();
        image = GetComponent<Image>();
    }

    //  refresh tooltip
    public void Refresh(SystmObject member)
    {
        systmObject = member;
        title.text = systmObject.tooltipTitle;
        text.text = systmObject.tooltipText;
    }

    //  detect click
    public void OnPointerClick(PointerEventData pointerEventData)
    {
        if (pointerEventData.button == PointerEventData.InputButton.Right)
        {
            tab.RightClicked(this);
        }

        if (pointerEventData.button == PointerEventData.InputButton.Left)
        {
            if (!details.activeInHierarchy)
            {
                Expand();
            }
            else
            {
                Contract();
            }
        }
    }

    //  show detailed info
    public void Expand()
    {
        rect.sizeDelta = new Vector2(rect.sizeDelta.x, 150);
        details.SetActive(true);
    }

    //  hide detailed info
    public void Contract()
    {
        details.SetActive(false);
        rect.sizeDelta = new Vector2(rect.sizeDelta.x, 50);
    }
}
