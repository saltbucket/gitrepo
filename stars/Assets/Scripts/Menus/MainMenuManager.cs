﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MainMenuManager : MonoBehaviour
{
    public GameManager game;

    public GameObject canvas;


    private void Awake()
    {

    }

    //  DETECT KEYCODE PRESSED, DOESN'T BELONG HERE
    void OnGUI()
    {
        if (Input.anyKeyDown)
        {
            Event e = Event.current;
            if (e != null && e.isKey && e.keyCode != KeyCode.None)
            {
                //Debug.Log("Detected key code: " + e.keyCode);

            }
        }
    }

    private void Update()
    {
        

    }

    public void StartButton()
    {
        game.StartGame();
        gameObject.SetActive(false);
    }

    public void OptionsButton()
    {

    }

}
