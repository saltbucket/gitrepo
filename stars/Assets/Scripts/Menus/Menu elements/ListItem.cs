﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ListItem : MonoBehaviour, IPointerClickHandler
{
    public UIList list;

    //  expandable panel
    public bool expandable;
    public Vector2 expandedSize;
    public GameObject details;

    public Text title;
    public Text text;
    public Image panelImage;

    private RectTransform rect;

    //  size when not expanded
    private Vector2 originalSize;

    private void Awake()
    {
        rect = GetComponent<RectTransform>();
        panelImage = GetComponent<Image>();
        originalSize = rect.sizeDelta;
        SetStyle();
    }

    //  set ui style
    private void SetStyle()
    {
        list.game.ui.uiStyle.SetText(title);
        list.game.ui.uiStyle.SetText(text);
        list.game.ui.uiStyle.SetTintedPanel(panelImage);
    }

    //  detect click
    public void OnPointerClick(PointerEventData pointerEventData)
    {
        //  right
        if (pointerEventData.button == PointerEventData.InputButton.Right)
        {
            list.RightClicked(this);
        }

        //  left
        if (pointerEventData.button == PointerEventData.InputButton.Left)
        {
            list.LeftClicked(this);
        }
    }

    //  toggle expand
    public void ToggleExpand()
    {
        if (details.activeInHierarchy)
        {
            Contract();
        }
        else
        {
            Expand();
        }
        gameObject.SetActive(false);
        gameObject.SetActive(true);
    }

    //  expand
    public virtual void Expand()
    {
        rect.sizeDelta = expandedSize;
        details.SetActive(true);
    }

    //  contract
    public virtual void Contract()
    {
        details.SetActive(false);
        rect.sizeDelta = originalSize;
    }
}
