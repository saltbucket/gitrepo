﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIList
{
    public Transform parent;
    public GameManager game;
    private GameObject listItemPanelPrefab;

    private List<ListItem> all = new List<ListItem>();
    private List<ListItem> selected = new List<ListItem>();

    protected UIStyles.UIStyle uiStyle;

    public UIList(Transform _parent, GameManager _game, GameObject _listItemPanelPrefab)
    {
        parent = _parent;
        game = _game;
        listItemPanelPrefab = _listItemPanelPrefab;
        uiStyle = game.ui.uiStyle;
    }

    //  add item
    public ListItem NewItem(GameObject itemPrefab)
    {
        ListItem listItem = game.ui.SpawnPrefab(listItemPanelPrefab, parent).GetComponent<ListItem>();
        all.Add(listItem);
        return listItem;
    }

    //  remove item
    protected virtual void RemoveItem(ListItem listItem)
    {
        all.Remove(listItem);
        game.ui.DestroyObject(listItem.gameObject);
    }

    //  clear
    protected void Clear()
    {
        List<ListItem> allCopy = new List<ListItem>(all);
        foreach (ListItem listItem in allCopy)
        {
            RemoveItem(listItem);
        }
    }

    //  item panel clicked
    public virtual void RightClicked(ListItem item) { }

    public virtual void LeftClicked(ListItem item) { }
    
    //  toggle object selected
    public virtual void ToggleSelect(ListItem item)
    {
        if (!selected.Contains(item))
        {
            Select(item);
        }
        else
        {
            Deselect(item);
        }
    }

    //  select panel
    protected virtual void Select(ListItem item)
    {
        SystmAPI.Generic.SetImageAlpha(item.panelImage, 10);
        selected.Add(item);
    }

    //  deselect panel
    protected virtual void Deselect(ListItem item)
    {
        SystmAPI.Generic.SetImageAlpha(item.panelImage, 0);
        selected.Remove(item);
    }

    //  deselect all
    protected void DeselectAll()
    {
        List<ListItem> selectedCopy = new List<ListItem>();
        selectedCopy.AddRange(selected);

        foreach (ListItem item in selectedCopy)
        {
            Deselect(item);
        }
        selected.Clear();
    }
}
