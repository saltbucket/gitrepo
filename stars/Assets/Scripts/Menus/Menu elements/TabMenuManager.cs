﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TabMenuManager
{
    public GameObject parent;
    public GameManager game;

    public List<Tab> allTabs;
    public Landable landable;

    private int currentIndex;
    private int previousIndex;

    public TabMenuManager(GameObject prent, GameManager gme)
    {
        parent = prent;
        game = gme;

        //  get all tabs
        allTabs = new List<Tab>();
        allTabs.AddRange(parent.GetComponentsInChildren<Tab>(includeInactive: true));


        //  construct and close tabs
        foreach (Tab tab in allTabs)
        {
            tab.manager = this;
            tab.Construct();
            tab.heading.transform.parent.GetComponent<Button>().onClick.AddListener(delegate { ChangeTab(tab); } );
            tab.Exit();
            tab.OnMenuClose();
        }

        //  set default tab
        ChangeTab(allTabs[2]);
    }

    //  enter
    public void Enter(Landable _landable = null)
    {
        //  already active
        if (parent.activeInHierarchy)
        {
            return;
        }

        //  landed on landable object
        if (landable != _landable)
        {
            landable = _landable;
        }

        //  tell tabs menu is open
        foreach (Tab tab in allTabs)
        {
            tab.OnMenuOpen();
        }

        //  open, pause enter tab
        parent.SetActive(true);
        game.Pause(true);
        allTabs[currentIndex].Enter();
    }

    //  exit
    public void Exit()
    {
        parent.SetActive(false);
        game.Pause(false);
        allTabs[currentIndex].Exit();

        //  clear landable if landed
        if (landable != null)
        {
            landable = null;
        }

        //  tell tabs menu has closed
        foreach (Tab tab in allTabs)
        {
            tab.OnMenuClose();
        }
    }
    
    //  next tab
    public void CycleTabs()
    {
        previousIndex = currentIndex;

        //  find next active tab
        for (int i = 0; i < allTabs.Count; i++)
        {
            //  increment
            currentIndex = SystmAPI.Generic.LoopList(currentIndex + 1, allTabs.Count);

            //  check
            if (allTabs[currentIndex].heading.IsActive())
            {
                allTabs[previousIndex].Exit();
                allTabs[currentIndex].Enter();
                return;
            }
        }
    }

    //  jump to tab
    public void ChangeTab(Tab tab)
    {
        Debug.Log("changing to " + tab.name + " tab");
        //  no change
        if (allTabs[currentIndex] == tab)
        {
            return;
        }
        else
        {
            allTabs[currentIndex].Exit();
        }

        //  find tab index and set current index
        for (int i = 0; i < allTabs.Count; i++)
        {
            if (allTabs[i] == tab)
            {
                currentIndex = i;
            }
        }
        allTabs[currentIndex].Enter();
    }

    //  input
    public void InputUpdate()
    {
        if (Input.GetKeyDown(game.settings.ship_BrowseTargets))
        {
            CycleTabs();
        }
        if (Input.GetKeyDown(game.settings.gameMenu))
        {
            Exit();
        }
    }
}