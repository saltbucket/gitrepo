﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : UIList
{
    private Dictionary<InventoryItem, GameObject> allItems;
    //  constructor
    public Inventory(Transform listParent, GameManager game, GameObject listItemPanelPrefab) : base(listParent, game, listItemPanelPrefab)
    {
        allItems = new Dictionary<InventoryItem, GameObject>();
    }

    //  add
    public void AddItem(GameObject itemPrefab, int count)
    {
        InventoryItem listItem = (InventoryItem)NewItem(itemPrefab);
        allItems[listItem] = itemPrefab;
        listItem.list = this;
        Item item = itemPrefab.GetComponent<Item>();
        listItem.count = count;
        listItem.image.sprite = item.image;
        listItem.title.text = item.itemName;
        listItem.text.text = "\nClass: " + item.itemClass + "\nFaction:";
    }

    //  remove
    protected override void RemoveItem(ListItem listItem)
    {
        allItems.Remove((InventoryItem) listItem);
        base.RemoveItem(listItem);
    }

    //  weapon
    public void AddWeapon(ShipWeapon weapon)
    {

    }

    //  ammo
    public void AddAmmo(Projectile ammo)
    {

    }

    //  right click
    public override void RightClicked(ListItem item)
    {
        ToggleSelect(item);
    }

    // left click
    public override void LeftClicked(ListItem item)
    {
        item.ToggleExpand();
    }
}
