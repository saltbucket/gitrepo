﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tab : MonoBehaviour
{
    [Header("Tab")]
    public Text heading;
    public TabMenuManager manager;

    //  monobehaviour constructor
    public virtual void Construct() { }

    //  open
    public virtual void OnMenuOpen() { }

    //  close
    public virtual void OnMenuClose() { }

    //  enter
    public virtual void Enter()
    {
        SystmAPI.Generic.SetTextAlpha(heading, 255);
        gameObject.SetActive(true);
    }
    
    //  exit
    public virtual void Exit()
    {
        gameObject.SetActive(false);
        SystmAPI.Generic.SetTextAlpha(heading, 100);
    }

    //  activate
    protected void Activate()
    {
        heading.gameObject.SetActive(true);
    }

    //  deactivate
    protected void Deactivate()
    {
        heading.gameObject.SetActive(false);
    }

    //  input
    private void Update()
    {
        if (Input.GetKeyDown(manager.game.settings.escape))
        {
            Escape();
        }
    }

    //  escape pressed
    protected virtual void Escape()
    {
        manager.Exit();
    }
}
