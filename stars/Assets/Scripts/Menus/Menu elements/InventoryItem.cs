﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryItem : ListItem
{
    public Item item;
    public int count;
    public Image image;
}
