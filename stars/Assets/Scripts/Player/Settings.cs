﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Settings
{
    [System.Serializable]
    public class ShipButtons
    {
        public KeyCode accelerate { get; set; }
        public KeyCode brake { get; set; }
        public KeyCode left { get; set; }
        public KeyCode right { get; set; }
    }

    public ShipButtons ship = new ShipButtons
    {
        accelerate = KeyCode.W,
        brake = KeyCode.S,
        left = KeyCode.A,
        right = KeyCode.D
    };

    public KeyCode ship_Accel = KeyCode.W;
    public KeyCode ship_Break = KeyCode.S;
    public KeyCode ship_Left = KeyCode.A;
    public KeyCode ship_Right = KeyCode.D;

    public KeyCode ship_Primary = KeyCode.Space;
    public KeyCode ship_Secondary = KeyCode.LeftControl;
    

    public KeyCode ship_BrowseTargets = KeyCode.Tab;
    public KeyCode ship_TargetFilters = KeyCode.T;

    public KeyCode ship_LockTarget = KeyCode.R;
    public KeyCode ship_command = KeyCode.C;


    public KeyCode ship_a1 = KeyCode.Alpha1;
    public KeyCode ship_a2 = KeyCode.Alpha2;
    public KeyCode ship_a3 = KeyCode.Alpha3;
    public KeyCode ship_a4 = KeyCode.Alpha4;
    public KeyCode ship_a5 = KeyCode.Alpha5;
    public KeyCode ship_a6 = KeyCode.Alpha6;
    public KeyCode ship_a7 = KeyCode.Alpha7;
    public KeyCode ship_a8 = KeyCode.Alpha8;
    public KeyCode ship_a9 = KeyCode.Alpha9;
    public KeyCode ship_a0 = KeyCode.Alpha0;

    public KeyCode commandGroup_attack = KeyCode.J;
    public KeyCode commandGroup_followLeader = KeyCode.K;
    public KeyCode commandGroup_command = KeyCode.C;

    public KeyCode fleetPanel = KeyCode.F;

    public KeyCode commandGroupTargets_uiPanel = KeyCode.G;

    public KeyCode gameMenu = KeyCode.T;

    public int[] ui_HighlightAlphas = new int[] { 75, 150, 255 };


    public KeyCode ship_k1 = KeyCode.Keypad1;

    public KeyCode escape = KeyCode.Escape;
    public KeyCode alternate = KeyCode.LeftShift;
    public KeyCode action = KeyCode.E;

    public Color colorGreen = new Color(0, 1, 0);
    public Color colorYellow = new Color(1, 1, 0);
    public Color colorRed = new Color(1, 0, 0);
    public Color32 colorBlue = new Color32(0, 175, 255, 255);

    public Color colorWhite = new Color(1, 1, 1);
    public Color colorBlack = new Color(0, 0, 0, 0.4f);


    public Color colorDarkGreen = new Color(0, 0.5f, 0);

    public Color colorGrey = new Color(0.5f, 0.5f, 0.5f);

    public UIStyles uiStyles = new UIStyles();
}
