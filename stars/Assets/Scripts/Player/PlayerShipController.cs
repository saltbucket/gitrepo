﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerShipController : Pilot {

    [Header("PlayerShipController")]
    public GameManager game;

    public Camera spaceCamera;
    public GameObject shipFollower;

    [HideInInspector]
    public Settings settings;
    public List<IPlayerInput> inputs = new List<IPlayerInput>();
    public List<IPlayerInput> inputsIterate = new List<IPlayerInput>();

    //  UI elements
    public UIManager ui;
    public bool tabMenuOpen;

    //AI
    public AIShipController autopilotAI;
    public bool autopilotOn;

    public void OnDrawGizmos()
    {
        if (ui.targetBrowser.currentTarget != null)
        {
            Gizmos.DrawLine(transform.position, ui.targetBrowser.currentTarget.transform.position);
        }
    }


    //  //
    #region Generic
    //  //

    private void Awake()
    {
        //  get current ship
        ship = transform.parent.GetComponent<Ship>();

        //  create instance of player settings
        settings = new Settings();

        //  get current ship
        //  this has to go in here/AIShipcontroller
        RefreshShip();
        
        //  get UI manager
        //ui = game.ui;
    }

    public override void RefreshShip()
    {
        base.RefreshShip();
        autopilotOn = false;
    }

    //  track previous position
    protected Vector3 currentPosition;
    private void GetPreviousPosition()
    {
        if (previousPosition != currentPosition)
        {
            previousPosition = currentPosition;
        }
        currentPosition = shipTrans.position;
    }
    
    //  get max zoom, larger ships let you zoom out further
    public float MaxZoom(bool commandMode)
    {
        if (!commandMode)
        {
            return ship.shipClass * 8;
        }
        else
        {
            return ship.shipClass * 16;
        }
    }

    //  center camera on player ship
    private void CenterCamera()
    {
        spaceCamera.transform.position = new Vector3
            (
                shipTrans.position.x,
                shipTrans.position.y,
                spaceCamera.transform.position.z
            );
    }

    //  get min zoonm
    //  TODO: make this do stuff
    public float MinZoom(bool commandMode)
    {
        return 0;
    }

    //  land
    public override void LandComplete(Landable landable)
    {
        ui.tabMenu.Enter(landable);
    }

    //  autopilot on
    private void AutoPilotOn()
    {
        autopilotOn = true;
        autopilotAI.gameObject.SetActive(true);
    }

    //  autopilot off
    private void AutoPilotOff()
    {
        autopilotOn = false;
        autopilotAI.aiState.Clear();
        autopilotAI.gameObject.SetActive(false);
    }

    #endregion

    //  //
    #region Fleet
    //  //

    //  add ship to fleet
    public void NewFleetMember(SystmObject member, int group = 0)
    {
        if (member == null || fleet.HasMember(member.pilot))
        {
            return;
        }
        
        fleet.AddMember(member.pilot, group);
        ui.fleetPanel.AddPanel(member, group);
        ui.fleetTab.AddMember(member, group);
    }

    //  remove ship from fleet
    public void RemoveFleetMember(SystmObject member, int group = 0)
    {
        if (member == null || !fleet.HasMember(member.pilot))
        {
            return;
        }

        fleet.RemoveMember(member.pilot);
        ui.fleetTab.RemoveMember(member);
    }

    //  push changes to fleet members e.g. hp/shields
    public override void UpdateMember(SystmObject member, bool dead)
    {
        ui.UpdateTargetStats(member, dead);
        if (dead)
        {
            RemoveFleetMember(member);
        }
    }

    //  //
    #region Commands
    //  //

    //  return chosen members or all members if none chosen
    private List<Pilot> GetMembers()
    {
        if (ui.membersChosen.Count == 0)
        {
            return fleet.AllMembers();
        }
        else
        {
            return SystmAPI.GetPilots(ui.membersChosen);
        }
    }

    //  order members to follow
    public void CommandFollowLeader()
    {
        fleet.FollowLeader(GetMembers());
    }

    //  order members to attack
    public void CommandAttack()
    {
        if (ui.membersChosen.Count == 0)
        {
            Debug.Log("CHOOSE members");
            //  TODO: help the player choose members
        }
        else if (ui.otherChosen.Count == 0)
        {
            Debug.Log(SystmAPI.GetPilots(ui.membersChosen));
            Debug.Log("CHOOSE OTHERS");
            //  TODO: help the player choose others
        }
        else
        {
            fleet.Attack(GetMembers(), SystmAPI.GetPilots(ui.otherChosen));
        }
    }

    #endregion

    #endregion

    //  //
    #region Targeting
    //  //

    //  when another ship targets you
    public override void TargetedBy(SystmObject sysObj, bool targeted)
    {
        //  whatever happens when something targets you
    }
    
    //  close the target browser
    private void ClearBrowser()
    {
        if (ui.targetBrowser.currentTarget != null)
        {
            ui.targetBrowser.currentTarget.Browsing(ship, false);
        }
        ui.targetBrowser.ClearBrowser();
    }

    //  lock target
    public override void NewTarget(SystmObject target)//, int group)//tageter
    {
        base.NewTarget(target);

        target.pilot.NewEnemy(this); //DEBUG
        NewEnemy(target.pilot);// DEBUG
    }

    // drop locked target
    public override void DropTarget()//int group)
    {
        base.DropTarget();
    }

    // target has changed, ran by the target
    public override void UpdateTarget(SystmObject target, bool dead)
    {
        base.UpdateTarget(target, dead);
        ui.UpdateTargetStats(target, dead);
    }

    #endregion

    //  //
    #region Input
    //  //
    
    //  get current mouse position
    public override Vector2 MousePosition()
    {
        return spaceCamera.ScreenToWorldPoint(Input.mousePosition);
    }
    
    //  return true if key down in list
    private bool GetKeysDown(List<KeyCode> buttons)
    {
        foreach(KeyCode button in buttons)
        {
            if (Input.GetKeyDown(button))
            {
                return true;
            }
        }
        return false;
    }

    //  return all keys used to move the ship
    private List<KeyCode> ShipMovementKeys()
    {
        return new List<KeyCode>
        {
            settings.ship.accelerate,
            settings.ship.brake,
            settings.ship.left,
            settings.ship.right
        };
    }

    //  //
    #region FixedUpdate
    //  //

    //  gameplay input for FixedUpdate()
    private void MainInputFixed()
    {
        GetPreviousPosition();

        //  movement
        if (!autopilot)
        {
            MovementControlInputFixed();
        }
    }

    private void MovementControlInputFixed()
    {
        //  turn
        if (Input.GetKey(settings.ship.left))
        {
            Left();
        }
        if (Input.GetKey(settings.ship.right))
        {
            Right();
        }

        //  stop normal
        if (Input.GetKey(settings.ship.brake))
        {
            if (ship.breaks == 1)
            {
                FaceBackwards();
            }
        }
    }

    private void FixedUpdate()
    {

        if (ui.commandMode.active)
        {
            //  command mode stuff for fixed update
        }
        else
        {
            MainInputFixed();
        }
    }

    #endregion

    //  //
    #region Update 
    //  //

    //  gameplay input for Update()
    private void MainInputUpdate()
    {
        //  movement/weapons
        if (!autopilot)
        {
            MovementControlInputUpdate();
            WeaponControlInputUpdate();
        }

        //  //
        #region Game Speed
        //  //
        //  1 - slow
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            if (Input.GetKey(KeyCode.LeftAlt))
            {
                Time.timeScale = 0.05f;
            }
        }

        //  2 - normal
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            if (Input.GetKey(KeyCode.LeftAlt))
            {
                Time.timeScale = 1f;
            }
        }

        //  3 - fast
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            if (Input.GetKey(KeyCode.LeftAlt))
            {
                Time.timeScale = 2f;
            }
        }

        #endregion


        //  //
        #region Selection
        //  //

        if (Input.GetKeyDown(settings.ship_LockTarget))
        {
            //  drop target
            if (Input.GetKey(settings.alternate))
            {
                DropTarget();
            }
            //  lock weapons on browsed target
            else if (ui.targetBrowser.currentTarget != ship.target)
            {
                NewTarget(ui.targetBrowser.currentTarget);
            }
        }

        #endregion
    }

    private void MovementControlInputUpdate()
    {
        //thrust animation control
        if (Input.GetKeyDown(settings.ship.accelerate))
        {
            Thrust(true);
        }
        if (Input.GetKeyUp(settings.ship.accelerate))
        {
            Thrust(false);
        }

        //  forward
        if (Input.GetKey(settings.ship.accelerate))
        {
            if (Input.GetKey(settings.alternate))
            {
                StrafeForward();
            }
            else
            {
                Accelerate(ship.speed);
            }
            previousPosition = shipTrans.position;

        }

        //  stop (gravity breaks)
        if (Input.GetKey(settings.ship.brake) && ship.breaks == 2)
        {
            Stop();
        }
    }

    private void WeaponControlInputUpdate()
    {
        //  primary
        if (Input.GetKeyDown(settings.ship_Primary))
        {
            FirePrimaryDown();
        }
        if (!Input.GetKey(settings.ship_Primary) && firingPrimary)
        {
            FirePrimaryUp();
        }

        //  secondary
        if (Input.GetKeyDown(settings.ship_Secondary))
        {
            FireSecondaryDown();
        }
        if (Input.GetKeyUp(settings.ship_Secondary))
        {
            FireSecondaryUp();
        }
    }
    
    private void AlphaNumercInputUpdate()
    {
        if (Input.GetKeyDown(settings.ship_a1))
        {
            GroupInputUpdate(0);
        }
        if (Input.GetKeyDown(settings.ship_a2))
        {
            GroupInputUpdate(1);
        }
    }

    //  what alphanumeric input does
    private void GroupInputUpdate(int group)
    {
        if (ui.tabParent.gameObject.activeInHierarchy)
        {
            ui.fleetTab.GroupInput(group);
            return;
        }
        if (Input.GetKey(settings.alternate))
        {
            ui.ChooseObjectList(fleet.GroupShips(group), ui.membersChosen);
        }
        else
        {
            ui.ClearChosen(ui.membersChosen);
            ui.ChooseObjectList(fleet.GroupShips(group), ui.membersChosen);
        }
    }
    
    private void ZoomInputUpdate(float wheelChange)
    {
        //  zoom based on wheel change within ships max/min zoom
        if (wheelChange != 0 && !ui.zoomLocked)
        {
            ui.StopZoom();
            
            spaceCamera.orthographicSize -= wheelChange * (spaceCamera.orthographicSize / 3);
            spaceCamera.orthographicSize = Mathf.Clamp(spaceCamera.orthographicSize, MinZoom(ui.commandMode.active), MaxZoom(ui.commandMode.active));
        }
    }

    private void DebugInputUpdate()//DEBUG
    {
        if (Input.GetKeyDown(KeyCode.V))
        {
            for (int i = 0; i < 20; i++)
            {
                //Debug.Log(game.resources.RandomWeaponItem("Neutral", 1).fireRate);
            }
        }

        if (Input.GetKeyDown(KeyCode.Keypad1)) //DEBU
        {
            ui.ChangeStyle(settings.uiStyles.Hospital());
        }
        if (Input.GetKeyDown(KeyCode.Keypad2)) //DEBU
        {
            ui.ChangeStyle(settings.uiStyles.Sewage());
        }
        if (Input.GetKeyDown(KeyCode.Keypad3)) //DEBU
        {
            ui.ChangeStyle(settings.uiStyles.Purple());
        }
        if (Input.GetKeyDown(KeyCode.Keypad4)) //DEBU
        {
            ui.ChangeStyle(settings.uiStyles.Acid());
        }
        if (Input.GetKeyDown(KeyCode.Keypad5)) //DEBU
        {
            ui.ChangeStyle(settings.uiStyles.BlueRubber());
        }

        if (Input.GetKeyDown(KeyCode.L))
        {
            ui.fleetPanel.CreateDebugGroup();
        }
    }//DEBUG

    void Update()
    {
        DebugInputUpdate();

        CenterCamera();
        AlphaNumercInputUpdate();
        if (ui.tabParent.activeInHierarchy)
        {
            ui.tabMenu.InputUpdate();
            return;
        }
        else if (Input.GetKeyDown(settings.gameMenu))
        {
            ui.tabMenu.Enter();
            return;
        }
        
        //  zoom
        //  <- mousewheel
        ZoomInputUpdate(Input.GetAxis("Mouse ScrollWheel"));

        //  open command menu
        //  <- C F R - escape
        if (Input.GetKeyDown(settings.commandGroup_command) && ui.NoMenusOpen())
        {
            ui.commandMenu.Enter();
        }

        //  opwn fleet panel
        //  <- TargetBrowser - action - escape - alhpanumeric
        if (Input.GetKeyDown(settings.fleetPanel) && ui.NoMenusOpen())
        {
            ui.fleetPanel.Enter();
        }

        //  //
        #region In Game
        //  //

        //  browse landable objects
        if (Input.GetKeyDown(KeyCode.G))
        {
            ui.targetBrowser.BrowseTargets(true, game.systm.landables);
        }

        //  when in game
        if (ui.NoMenusOpen())
        {
            //AlphaNumercInputUpdate();
            //  esc
            if (Input.GetKeyDown(settings.escape))
            {
                //  clear chosen others
                if (ui.OthersChosen())
                {
                    ui.ClearChosen(ui.otherChosen);
                }
                //  else clear chosen members
                else if (ui.MembersChosen())
                {
                    ui.ClearChosen(ui.membersChosen);
                }
                //  else clear target browser
                else if (ui.targetBrowser.currentTarget!= null)
                {
                    ui.ResetBrowsing();
                }
            }
            //action
            if (Input.GetKeyDown(settings.action))
            {
                //  land on target
                if (ui.targetBrowser.currentTarget.landable)
                {
                    AutoPilotOn();
                    Landable landable = ui.targetBrowser.currentTarget.GetComponent<Landable>();
                    autopilotAI.Land(landable);
                }
                //  select target
                else
                {
                    ui.ChooseHighlighted();
                }
            }
        }

        //  cancel autopilot
        if (autopilotOn && GetKeysDown(ShipMovementKeys()))
        {
            AutoPilotOff();
        }

        //  command mode
        if (Input.GetKeyDown(KeyCode.Q))
        {
            ui.commandMode.EnterCommandMode();
        }

        //  unpaused
        if (!game.paused)
        {
            //  track ship position with follower object
            shipFollower.transform.position = shipTrans.position;

            //  in game input updates
            MainInputUpdate();
        }

        #endregion

        //  update input active input states - must stay at the end of PlayerShipController.Update()
        foreach (IPlayerInput input in inputsIterate)
        {
            input.InputUpdate();
        }
        inputsIterate.Clear();
        inputsIterate.AddRange(inputs);
    }



    #endregion

    #endregion
    
    //  //
    #region AI Updates
    //  //

    //  every 0.1 seconds
    public override void SlowUpdate()
    {
        autopilotAI.SlowUpdate();
    }

    //  every 0.5 seconds
    public override void SlowerUpdate()
    {
        autopilotAI.SlowerUpdate();
    }

    #endregion
}
